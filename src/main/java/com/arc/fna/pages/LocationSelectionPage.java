package com.arc.fna.pages;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.w3c.dom.Document;

import com.arc.fna.utils.ARCFNAAppUtils;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

@Listeners(EmailReport.class)

public class LocationSelectionPage extends LoadableComponent<LocationSelectionPage> {
	
	public static AppiumDriver<MobileElement> driver;
	private String cachedPageSource = null;
	private Document cachedDocument = null;
	private boolean isPageLoaded;
	
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeStaticText[2]")
	
	WebElement btnConfirmDefaultLoc;
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeStaticText[2]")
	
	List<WebElement> btnConfirmDefaultLoc1;
	
	
	public LocationSelectionPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}


	@Override
	protected void load() {
		isPageLoaded = true;
		 //ARCFNAAppUtils.waitForElement(driver, btnDontAllow);
	}
	
	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	
	public void clickDontAllow() throws InterruptedException
	{
		int btnConfirmDefaultLoc1count = btnConfirmDefaultLoc1.size();
		if(btnConfirmDefaultLoc1count != 1)
		{
		Log.message("Alert message for Allow / Don't Allow appeared");
		driver.switchTo().alert().dismiss();
		Log.message("Don't Allow button is clicked");
		}
		Log.message("Alert message for Allow / Don't Allow is not appeared");
		ARCFNAAppUtils.waitForElement(driver, btnConfirmDefaultLoc, 30);
		btnConfirmDefaultLoc.click();
		Thread.sleep(3000);
		Log.message("Confirm clicked for USA");
		driver.switchTo().alert().accept();
		Thread.sleep(5000);
		Log.message("YES cliked for confirm message");
	}
	
	private String getPageSource() {
		if (cachedPageSource == null) {
			cachedPageSource = driver.getPageSource();
		}
		return cachedPageSource;
	}
	private void setPageSource(String pageSource) {
		cachedPageSource = pageSource;
		cachedDocument = null;
	}
	private void clearPageSource() {
		cachedPageSource = null;
		cachedDocument = null;
	}
	
	

}

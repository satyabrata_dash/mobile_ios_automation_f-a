package com.arc.fna.pages;

import java.io.IOException;
import java.util.List;

//import org.aspectj.asm.IProgramElement.Accessibility;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;
import org.w3c.dom.Document;

import com.arc.fna.utils.ARCFNAAppUtils;
import com.arc.fna.utils.LoadObjectProperties;
import com.arc.fna.utils.LoadPropertiesFromFileSystem;
import com.arcautoframe.utils.EnvironmentPropertiesReader;
import com.arcautoframe.utils.Log;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.pagefactory.iOSFindBys;

public class FNALoginPage extends LoadableComponent<FNALoginPage>
{
	
	public static AppiumDriver<MobileElement> driver;
	private String cachedPageSource = null;
	private Document cachedDocument = null;
	private boolean isPageLoaded;
	private static LoadPropertiesFromFileSystem orProperty = LoadPropertiesFromFileSystem.getInstance();
	
	
	/* IOS App Element */
	
	@iOSFindBy(accessibility = "OK")
	WebElement loginFailedOk1;
	
	@iOSFindBy(accessibility = "OK")
	List<WebElement> loginFailedOk;
	
	@iOSFindBy(accessibility = "wifi-button")
	WebElement btnWifi;
	
	@iOSFindBy(accessibility = "com.ARC.PlanwellCollaboration")
	WebElement fnaButtonWifiOff;
	
	@iOSFindBy(accessibility="ForceUpdateBtnUpdate")
	WebElement btnEmailArchiveAdd;
	
	@iOSFindBy(accessibility="ForceUpdateBtnUpdate")
	List<WebElement> btnEmailArchiveAdds;
	
	@iOSFindBy(accessibility="LoginUsernameTextfield")
	@CacheLookup
	WebElement txtBoxUserName;

	@iOSFindBy(accessibility="LoginPasswordTextfield")
	@CacheLookup
	WebElement txtBoxPassword;
	
	@iOSFindBy(accessibility="Sign in")
	@CacheLookup
	WebElement btnLogin;
	
	@iOSFindBy(accessibility="skip")
	WebElement btnSkip;
	
	@iOSFindBy(accessibility="ProjectHelpSkip")
	WebElement btnSkipAfterLogin;
	
	@iOSFindBy(accessibility="skip")
	List <WebElement> btnSkips;
	
	@iOSFindBy(accessibility="ProjectHelpSkip")
	List <WebElement> btnSkipsAfterLogin;
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		ARCFNAAppUtils.waitForElement(driver, txtBoxUserName);
	}
	
	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	
	public FNALoginPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	/** Method written for successful login.
	 * @throws IOException 
	 * @throws InterruptedException 
	 * 
	 */
	@SuppressWarnings("unused")
	public CollectionListPage loginWithValidCredential() throws IOException, InterruptedException
	{		
		
		Log.message("Login for FNA mobile app is initiated.");
		ARCFNAAppUtils.waitForElement(driver, txtBoxUserName, 40);
		txtBoxUserName.clear();
		txtBoxUserName.sendKeys(ARCFNAAppUtils.testDataReading("username"));
		Log.message(ARCFNAAppUtils.testDataReading("username")+" "+ "has been entered as Username");
		
		ARCFNAAppUtils.waitForElement(driver, txtBoxPassword, 40);
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(ARCFNAAppUtils.testDataReading("password"));
		Log.message(ARCFNAAppUtils.testDataReading("password")+" "+ "has been entered as password");
		
		ARCFNAAppUtils.waitForElement(driver, btnLogin, 20);
		btnLogin.click();
		Log.message("Login button clicked");
		
		ARCFNAAppUtils.waitTill(2000);
		
		//if(btnLogin.isDisplayed())
		
		if(loginFailedOk.size()>0)	
		{
			try
			{
				//((MobileDriver) driver).swipe(509,761, 625, 285, 1000);
				TouchAction t1 = new TouchAction(driver);
				t1.longPress(962, 0).moveTo(962, 228).perform().release();
				ARCFNAAppUtils.waitTill(2000);
				ARCFNAAppUtils.waitForElement(driver, btnWifi, 30);
				if(btnWifi.getAttribute("value").equals("0"))
				{
				btnWifi.click();
				ARCFNAAppUtils.waitTill(2000);
				TouchAction t2 = new TouchAction(driver);
				t2.press(389, 433).perform().release();
				ARCFNAAppUtils.waitTill(2000);
				//ARCFNAAppUtils.waitForElement(driver, fnaButtonWifiOff, 20);
				//fnaButtonWifiOff.click();
				//ARCFNAAppUtils.waitTill(2000);
				ARCFNAAppUtils.waitForElement(driver, loginFailedOk1, 30);
				loginFailedOk1.click();
				btnLogin.click();
				}
				/*else
				{
					ARCFNAAppUtils.waitForElement(driver, fnaButtonWifiOff, 20);
					fnaButtonWifiOff.click();
					btnLogin.click();
				}*/
				ARCFNAAppUtils.waitTill(3000);
					
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		ARCFNAAppUtils.waitForElement(driver, btnSkipAfterLogin, 10);
		int cntSkip = btnSkipsAfterLogin.size();
		if(cntSkip>0)
		{
		btnSkipAfterLogin.click();
		Log.message("Skip button clicked");
		}
		ARCFNAAppUtils.waitForElement(driver, btnEmailArchiveAdd, 5);
		int cntAdd = btnEmailArchiveAdds.size();
		if(cntAdd>0)
		{
			btnEmailArchiveAdd.click();
		}
		Log.message("SKYSITE FNA Login successfull in DEVICE / MOBILE");
		return new CollectionListPage(driver).get();
		
	}
	
	public CollectionListPage loginWithValidCredentialWithoutSkip() throws IOException, InterruptedException
	{
		ARCFNAAppUtils.waitForElement(driver, txtBoxUserName, 40);
		txtBoxUserName.clear();
		txtBoxUserName.sendKeys(ARCFNAAppUtils.testDataReading("username"));
		Log.message(ARCFNAAppUtils.testDataReading("username")+" "+ "has been entered as Username");
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(ARCFNAAppUtils.testDataReading("password"));
		Log.message(ARCFNAAppUtils.testDataReading("password")+" "+ "has been entered as password");
		btnLogin.click();
		return new CollectionListPage(driver).get();
	}
	
	public void launchApplication()
	{
		driver.get("https://app.skysite.com/Account/UserCommonLogin");
	}
	
	
	
}

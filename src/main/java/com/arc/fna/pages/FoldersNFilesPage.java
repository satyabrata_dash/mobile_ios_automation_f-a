package com.arc.fna.pages;

import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.sql.DriverAction;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FileUtils;
//import org.aspectj.asm.IProgramElement.Accessibility;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import org.testng.Assert;
import org.w3c.dom.Document;
import org.sikuli.script.Button;
import org.sikuli.script.FindFailed;
import org.sikuli.script.ImageLocator;
import org.sikuli.script.ImagePath;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;

import com.arc.fna.utils.ARCFNAAppUtils;
import com.arc.fna.utils.LoadPropertiesFromFileSystem;
import com.arcautoframe.utils.EnvironmentPropertiesReader;
import com.arcautoframe.utils.Log;
import com.gargoylesoftware.htmlunit.javascript.host.event.TouchEvent;
//import com.sun.jna.platform.FileUtils;
import com.hp.hpl.jena.ontology.ObjectProperty;
import com.hp.hpl.jena.rdf.model.Resource;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.TouchAction;
import io.appium.java_client.TouchShortcuts;
//import io.appium.java_client.android.Connection;
import io.appium.java_client.android.HasNetworkConnection;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindAll;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.pagefactory.iOSFindBys;

public class FoldersNFilesPage extends LoadableComponent<FoldersNFilesPage> {

	private static LoadPropertiesFromFileSystem objectProperty = LoadPropertiesFromFileSystem.getInstance();

	// Properties locators = new Properties();

	public static AppiumDriver<MobileElement> driver;
	// public static MobileDriver driver;
	private String cachedPageSource = null;
	private Document cachedDocument = null;
	private boolean isPageLoaded;

	
	@iOSFindBy(accessibility="DrawerControlBtn")
	WebElement btnMenu;
	
	@iOSFindBy(accessibility="Equipment")
	WebElement btnEquipment;
	
	@iOSFindBy(accessibility="CANCEL")
	WebElement btnCancelMoveFile;

	@iOSFindBy(accessibility = "moveCancel")
	WebElement btnCancelMoveFolder;

	@iOSFindBy(xpath = "//XCUIElementTypeCollectionView/XCUIElementTypeOther")
	WebElement renameFolderTextBox;

	@iOSFindBy(accessibility = "Rename")
	WebElement btnFolderNameRename;

	@iOSFindBy(accessibility = "Return to Facilities & Archive")
	WebElement btnReturnFacilityArchiveGooglePage;

	@iOSFindBy(accessibility = "RotateRightButton")
	WebElement rotationRightButton;

	@iOSFindBy(accessibility = "RotationControlSaveButton")
	WebElement rotationSaveButton;

	@iOSFindBy(accessibility = "offlineBack")
	WebElement closeViewerFolderLevel;

	@iOSFindBy(accessibility = "CLOSE")
	WebElement fileInfoClose;

	@iOSFindBy(accessibility = "FolderCollectionView")
	WebElement folderListPanel;

	@iOSFindBy(accessibility = "Delete Draft")
	WebElement btnDeleteMailDraft;

	@iOSFindBy(accessibility = "PDFViewerRenderView")
	WebElement viewerContainer;

	@iOSFindBy(accessibility = "Deselect")
	WebElement btnDeselect;

	@iOSFindBy(accessibility = "FileStatusLabel")
	WebElement captionSyncOffline;

	@iOSFindBy(accessibility = "CloseBtn")
	WebElement btnCloseViewer;

	@iOSFindBy(accessibility = "MakeOfflineSwitchForFileList")
	WebElement btnSyncOffline;

	@iOSFindBy(accessibility = "MakeOfflineSwitchForFileList")
	List<WebElement> btnSyncOfflines;

	@iOSFindBy(accessibility = "Google")
	WebElement capGoogle;

	@iOSFindBy(accessibility = "ExternalLink")
	WebElement btnExternalhyperlink;

	@iOSFindBy(accessibility = "HyperlinkTextView")
	WebElement txtHyperlinkTextView;

	@iOSFindBy(accessibility = "External link")
	WebElement btnExternalLink;

	@iOSFindBy(accessibility = "HeaderTitle")
	WebElement btnRevisionhistory;

	@iOSFindBy(accessibility = "FileListInfoBtn")
	WebElement btnFileListInfo;

	@iOSFindBy(accessibility = "FileListInfoBtn")
	List<WebElement> btnFileListInfos;

	@iOSFindBy(accessibility = "cloud infolink")
	WebElement btnCloudhyperlink;

	@iOSFindBy(accessibility = "circle")
	WebElement btnCirclehyperlink;

	@iOSFindBy(accessibility = "Done")
	WebElement btnTextNoteDone;

	@iOSFindBy(accessibility = "textviewNote")
	WebElement txtViewNote;

	@iOSFindBy(accessibility = "PDFViewerCalloutTextfiled")
	WebElement txtViewNote1;

	@iOSFindBy(accessibility = "txtViewNote")
	WebElement txtViewNote3;

	@iOSFindBy(accessibility = "PDFViewerCalloutTextfiled")
	WebElement txtViewNote2;

	@iOSFindBy(accessibility = "note infolink")
	WebElement btnTextNoteInfoLink;

	@iOSFindBy(accessibility = "text infolink")
	WebElement btnTextInfoSub;

	@iOSFindBy(accessibility = "marker infolink")
	WebElement btnMarkerMarkupSub;

	@iOSFindBy(accessibility = "line infolink")
	WebElement btnLineMarkupline;

	@iOSFindBy(accessibility = "cloud infolink")
	WebElement btnCloudInfolink;

	@iOSFindBy(accessibility = "circle")
	WebElement btnCircle;

	@iOSFindBy(accessibility = "Cut")
	WebElement btnCut1;

	@iOSFindBy(accessibility = "Select All")
	WebElement btnSelectAll1;

	@iOSFindBy(accessibility = "RotationControlCancelButton")
	WebElement btnRotationCancel;

	@iOSFindBy(accessibility = "RotationControlSaveButton")
	WebElement btnRotationSave;

	@iOSFindBy(accessibility = "ExportMailSend")
	WebElement btnMailSend;

	@iOSFindBy(accessibility = "ExportTextfield")
	WebElement txtFieldTo;

	@iOSFindBy(accessibility = "ExportPDFbtn")
	WebElement btnPDFDocumentType;

	@iOSFindBy(accessibility = "Document")
	WebElement btnPDFDocument;

	@iOSFindBy(accessibility = "ExportGroupBtn")
	WebElement btnExportSend;

	@iOSFindBy(accessibility = "Set Active")
	WebElement btnSetActiveCalibrator;

	@iOSFindBy(accessibility = "Set Active")
	List<WebElement> btnSetActiveCalibrators;

	@iOSFindBy(accessibility = "Metric")
	WebElement tabCalibratorMetric;

	@iOSFindBy(accessibility = "Centimeter")
	WebElement txtCalibratorCentiMeter;

	@iOSFindBy(accessibility = "Meter")
	WebElement txtCalibratorMeter;

	@iOSFindBy(accessibility = "Recalibrate")
	WebElement btnCalibratorRecalibrate;

	@iOSFindBy(accessibility = "Clear all")
	WebElement btnCalibratorClearall;

	@iOSFindBy(accessibility = "trash legacy")
	WebElement btnCalibratorDelete;

	@iOSFindBy(accessibility = "Settings")
	WebElement btnCalibratorSetting;

	@iOSFindBy(accessibility = "measure area")
	WebElement btnCalibratorMeasureArea;

	@iOSFindBy(accessibility = "measure freehand")
	WebElement btnCalibratorMeasureFreehand;

	@iOSFindBy(accessibility = "measure circle")
	WebElement btnCalibratorMeasureCircle;

	@iOSFindBy(accessibility = "measure rect")
	WebElement btnCalibratorMeasureRect;

	@iOSFindBy(accessibility = "measure line")
	WebElement btnCalibratorMeasureLine;

	@iOSFindBy(accessibility = "LoadMarkupBtn")
	WebElement btnHideAll;

	@iOSFindBy(accessibility = "LoadMarkupBtn")
	WebElement btnViewAll;

	@iOSFindBy(accessibility = "LoadMarkupBtn")
	WebElement btnViewAllWithOutCash;

	@iOSFindBy(accessibility = "PDFViewerCalibrationMessurementTextfieldSuper")
	WebElement txtFeet;

	@iOSFindBy(accessibility = "PDFViewerCalibrationMessurementTextfieldSuper")
	WebElement txtFeetWithOutCache;

	@iOSFindBy(accessibility = "PDFViewerCalibrationMessurementTextfieldSub")
	WebElement textInches;

	@iOSFindBy(accessibility = "PDFViewerCalibrationMessurementTextfieldSub")
	WebElement textInchesWithOutCache;

	@iOSFindBy(accessibility = "Automation_Testing2")
	WebElement hyperlinkFolderName;

	@iOSFindBy(accessibility = "Automation_Testing2_1.0")
	WebElement hyperlinkFolderName1;

	@iOSFindBy(accessibility = "HyperLinkLinkBtn")
	WebElement btnLink;

	@iOSFindBy(accessibility = "hyperlinkLinkBtn")
	WebElement btnLink1;

	@iOSFindBy(accessibility = "MOVE")
	WebElement btnMove1;

	@iOSFindBy(accessibility = "Move")
	WebElement btnMove;

	@iOSFindBy(accessibility = "Cancel")
	WebElement btnCancelIOSMailUI;

	@iOSFindBy(accessibility = "Share")
	WebElement btnShare;

	@iOSFindBy(accessibility = "Sample Facility- Technology Center")
	WebElement rootFolderNavigation;

	@iOSFindBy(accessibility = "AlbumListSyncSwitch")
	WebElement btnSync;

	@iOSFindBy(accessibility = "PhotoCapture")
	WebElement btnPhotoCapture;

	@iOSFindBy(accessibility = "Use Photo")
	WebElement btnUsePhoto;

	@iOSFindBy(accessibility = "Remove")
	WebElement uploadedPhoto;

	@iOSFindBy(accessibility = "Upload files")
	WebElement btnUploadFile;

	@iOSFindBy(accessibility = "Automation_Testing3")
	WebElement btncreatedFolder;

	@iOSFindBy(accessibility = "Automation_Testing5")
	WebElement btncreatedFolder5;

	@iOSFindBy(accessibility = "Automation_Testing5")
	List<WebElement> btncreatedFolders5;

	@iOSFindBy(accessibility = "Automation_Testing5_Changed")
	WebElement btncreatedFolderNameChanged5;

	@iOSFindBy(accessibility = "Automation_Testing5_Changed")
	List<WebElement> btncreatedFoldersNameChanged5;

	@iOSFindBy(accessibility = "Automation_Testing4")
	WebElement btncreatedFolder1;

	@iOSFindBy(accessibility = "Automation_Testing3")
	List<WebElement> btncreatedFolders;

	@iOSFindBy(accessibility = "Automation_Testing4")
	List<WebElement> btncreatedFolders1;

	@iOSFindBy(accessibility = "Create folder")
	WebElement btncreateFolder;

	@iOSFindBy(accessibility = "moreIcon")
	WebElement btnThreeDotted;

	@iOSFindBy(accessibility = "Select")
	WebElement btnSelect;

	@iOSFindBy(accessibility = "Delete")
	WebElement btnDelete;

	@iOSFindBy(accessibility = "HyperlinkCreatePageSelectTextField")
	WebElement pageNoTextBox;

	@iOSFindBy(accessibility = "▶")
	WebElement hyperlinkPagination;

	@iOSFindBy(accessibility = "Go")
	WebElement hyperlinkGo;

	@iOSFindBy(accessibility = "Automation_Testing1") // "Automation_Testing1")
	WebElement btnParentFolder;

	@iOSFindBy(accessibility = "Automation_Testing2") // "Automation_Testing1")
	WebElement btnChildFolder1;

	@iOSFindBy(accessibility = "Automation_Testing2_1.0") // "Automation_Testing1")
	WebElement btnChildFolder2;

	@iOSFindBy(accessibility = "Automation_Testing2_1.0.1") // "Automation_Testing1")
	WebElement btnChildFolder3;

	@iOSFindBy(accessibility = "PDFViewerMainContentView")
	WebElement docViewer;

	@iOSFindBy(accessibility = "skip")
	WebElement btnSkip;

	@iOSFindBy(accessibility = "Skip")
	WebElement btnSkipAfterFolder;

	@iOSFindBy(accessibility = "Skip")
	List<WebElement> btnSkipsAfterFolder;

	@iOSFindBy(accessibility = "ViewerHelpSkip")
	WebElement btnSkipFileOpening;

	@iOSFindBy(accessibility = "ViewerHelpSkip")
	List<WebElement> btnSkipFilesOpening;

	@iOSFindBy(accessibility = "skip")
	List<WebElement> btnSkips;

	@iOSFindBy(accessibility = "Release PDF.PDF")
	WebElement btnFile;

	@iOSFindBy(accessibility = "A-101.pdf")
	WebElement btnFile1;

	@iOSFindBy(accessibility = "Test OCR 3.pdf")
	WebElement btnFile2;

	@iOSFindBy(accessibility = "(R-1) RotatePDFPart-1.pdf")
	WebElement hyperLinkfileName1;

	@iOSFindBy(accessibility = "(R-1) Test OCR 3.pdf")
	WebElement hyperLinkfileName2;

	@iOSFindBy(accessibility = "Test OCR 3.pdf")
	WebElement hyperLinkfileName7;

	@iOSFindBy(accessibility = "(R-1) Release PDF.PDF")
	WebElement hyperLinkfileName3;

	@iOSFindBy(accessibility = "(R-2) Release PDF.PDF")
	WebElement hyperLinkfileName5;

	@iOSFindBy(accessibility = "(R-1) A-101.pdf")
	WebElement hyperLinkfileName4;

	@iOSFindBy(accessibility = "HyperlinkInfoPreviewButton")
	MobileElement fileNameThumbnail;

	@iOSFindBy(accessibility = "marker infolink")
	WebElement btnMarker;

	@iOSFindBy(accessibility = "rectangle infolink")
	WebElement btnRectangleInfolink;

	@iOSFindBy(accessibility = "square")
	WebElement btnRectangleInfolinkSquare;

	@iOSFindBy(accessibility = "AnnotationLayoutView")
	WebElement inputCanvas;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeScrollView/XCUIElementTypeOther")
	WebElement btnMinimize;

	@iOSFindBy(accessibility = "arrowRight New")
	WebElement btnRightArrow;

	@iOSFindBy(accessibility = "arrowLeft New")
	WebElement btnLeftArrow;

	@iOSFindBy(accessibility = "(R-1) Release PDF.PDF")
	WebElement labFileName;

	@iOSFindBy(accessibility = "Save")
	@CacheLookup
	WebElement btnAnnotationSave;

	@iOSFindBy(accessibility = "SaveMarkupTextBox")
	WebElement txtBoxMarkupSaving;

	@iOSFindBy(xpath = "//[@name='Save'])")
	WebElement btnSaveMarkupName;

	@iOSFindBy(accessibility = "ShowMarkUpDropDownButton")
	WebElement btnViewDropDown;

	@iOSFindBy(accessibility = "PDFViewerMarkupTable")
	WebElement selectSavedMarkup;

	@iOSFindBy(accessibility = "PDFViewerMarkupTable")
	List<WebElement> allSelectSavedMarkup;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell")
	List<WebElement> selectSavedMarkupList;

	@iOSFindBy(accessibility = "back")
	WebElement btnBack;

	@iOSFindBy(xpath = "//XCUIElementTypeOther[3]/XCUIElementTypeOther[2]")
	WebElement markupList;

	@iOSFindBy(xpath = "//XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther")
	WebElement entireWindow;

	@iOSFindBy(accessibility = "toolbar rotation")
	WebElement rotationToolbar;

	@iOSFindBy(accessibility = "rotate left")
	WebElement btnRotateLeft;

	@iOSFindBy(accessibility = "rotate right")
	WebElement btnRotateRight;

	@iOSFindBy(accessibility = "Delete")
	WebElement btnDeleteMarkup;

	@iOSFindBy(xpath = "//XCUIElementTypeCell")
	WebElement allSavedMarkup;

	@iOSFindBy(accessibility = "YES")
	WebElement btnYesDeleteMarkup;

	@iOSFindBy(accessibility = "line infolink")
	WebElement btnLineMarkup;

	@iOSFindBy(accessibility = "arrowAnnotation")
	WebElement btnLineMarkupArrow;

	@iOSFindBy(accessibility = "marker infolink")
	WebElement btnMarkerMarkup;

	@iOSFindBy(accessibility = "text infolink")
	WebElement btnTextMarkup;

	@iOSFindBy(accessibility = "callout annot")
	WebElement btnTextMarkupCallout;

	@iOSFindBy(accessibility = "hyperlink 1")
	WebElement btnHyperlinkMarkup;

	@iOSFindBy(accessibility = "square")
	WebElement btnHyperlinkMarkupSquare;

	@iOSFindBy(accessibility = "Search or paste link")
	WebElement btnHyperlinkFileSearch;

	@iOSFindBy(accessibility = "Search or paste link")
	WebElement btnHyperlinkFileSearchWithOutCache;

	@iOSFindBy(accessibility = "PDFViewerCalloutTextfiled")
	WebElement txtAreaCalloutMarkup;

	@iOSFindBy(accessibility = "Done")
	WebElement btnDoneCallout;

	@iOSFindBy(accessibility = "marker infolink")
	WebElement btnMarkerFree;

	@iOSFindBy(accessibility = "freehand")
	WebElement btnMarkerFreehand;

	@iOSFindBy(accessibility = "RotatePDFPart-1.pdf")
	WebElement fileToBeLinked;

	@iOSFindBy(accessibility = "A-101.pdf")
	WebElement fileToBeLinked1;

	@iOSFindBy(accessibility = "pdf-thumbnail")
	WebElement hyperlinkJumpTofile;

	@iOSFindBy(accessibility = "Automation_Testing1")
	WebElement searchedFolder1;

	@iOSFindBy(accessibility = "Automation_Testing2")
	WebElement searchedFolder2;

	@iOSFindBy(accessibility = "FolderTopLbl")
	WebElement searchedFolder;

	@iOSFindBy(accessibility = "Automation_Testing2_1.0")
	WebElement searchedFolder3;

	@iOSFindBy(accessibility = "Automation_Testing2_1.0.1")
	WebElement searchedFolder4;

	@iOSFindBy(accessibility = "RotatePDFPart-1.pdf")
	WebElement searchedFile;

	@iOSFindBy(accessibility = "Test OCR 3.pdf")
	WebElement searchedFile1;

	@iOSFindBy(accessibility = "Release PDF.PDF")
	WebElement searchedFile2;

	@iOSFindBy(accessibility = "Measurement Calibrator")
	WebElement btnCalibrator;

	@iOSFindBy(accessibility = "Measurement Calibrator")
	WebElement btnCalibratorWithOutCash;

	@iOSFindBy(accessibility = "calibration")
	WebElement btnCalibratorSubOption;

	@iOSFindBy(accessibility = "calibration")
	WebElement btnCalibratorSubOptionWithOutCash;

	@iOSFindBy(accessibility = "PDFViewerCalibrationMessurementDoneButton")
	WebElement btnCalibratorDone;

	// @iOSFindBy(accessibility = "PDFViewerCalibrationMessurementDoneButton")
	// WebElement btnCalibratorDoneWithOutCache;

	@iOSFindBy(accessibility = "PDFViewerCalibrationMessurementTextfield")
	WebElement txtLengthCalibrator;

	@iOSFindBy(accessibility = "multi select")
	WebElement btnMultiSelect;

	@iOSFindBy(accessibility = "Select all")
	WebElement btnSelectAll;

	@iOSFindBy(accessibility = "Remove")
	WebElement btnRemove;

	@iOSFindBy(accessibility = "Clear text")
	WebElement btnClrTxt;

	@iOSFindBy(accessibility = "arrowRight New")
	WebElement paginationArrowRight;

	/*
	 * Author: Subhagat Below locators are created for photo tagging
	 */
	@iOSFindBy(accessibility = "tagIcon")
	WebElement tagIcon;

	@iOSFindBy(accessibility = "Change icon")
	WebElement tag_ChangeIcon;

	@iOSFindBy(xpath = "(//XCUIElementTypeButton[@name='Cancel'])[2]")
	WebElement tag_CancelBtn;

	@iOSFindBy(xpath = "((//XCUIElementTypeButton[@name='Save'])[2]")
	WebElement tag_SaveBtn;

	@iOSFindBy(accessibility = "add")
	WebElement tag_AddPhotoVideo;

	@iOSFindBy(id = "Search")
	WebElement tag_SearchTxtBox;

	@iOSFindBy(accessibility = "Done")
	WebElement doneBtn;

	@Override
	protected void load() {
		isPageLoaded = true;
		// ARCFNAAppUtils.waitForElement(driver, btnDontAllow);
	}

	@Override
	protected void isLoaded() {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}

	public FoldersNFilesPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	public void openParentFolder() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnParentFolder, 20);
		btnParentFolder.click();
		Log.message("Parent folder clicked");
		ARCFNAAppUtils.waitForElement(driver, btnSkipAfterFolder, 30);
		int noSkipBtn = btnSkipsAfterFolder.size();
		if (noSkipBtn > 0) {
			btnSkipAfterFolder.click();
		}
		Log.message("Desired folder is opened");
	}

	public void openParentFolderWithoutSkip() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnParentFolder, 20);
		btnParentFolder.click();
	}

	public void openFile() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnFile, 50);
		btnFile.click();
		Log.message("Desired file opened");

		ARCFNAAppUtils.waitForElement(driver, btnSkipFileOpening, 30);
		int skipBtnCount = btnSkipFilesOpening.size();
		if (skipBtnCount > 0) {
			btnSkipFileOpening.click();
		}
		ARCFNAAppUtils.waitTill(4000);
		// Thread.sleep(1000);
		Log.message("Desired file is opened");
	}

	public void zoomingFile() throws FindFailed, InterruptedException {
		ARCFNAAppUtils.waitTill(4000);
		// TouchAction action1 = new TouchAction(driver).longPress(457,
		// 391).moveTo(0,-50).release().perform();
		//// TouchAction action1 = new TouchAction(driver).longPress(481,
		// 407).moveTo(479, 406).release().perform();
		TouchAction action1 = new TouchAction(driver).longPress(495, 350).moveTo(495, 345).release().perform();
		// driver.zoom(x, y);

		// TouchAction acction2 = new TouchAction(driver).longPress(457,
		// 391).moveTo(50,0).release().perform();
		//// TouchAction action2 = new TouchAction(driver).longPress(481,
		// 407).moveTo(483, 408).release().perform();
		TouchAction action2 = new TouchAction(driver).longPress(486, 472).moveTo(486, 482).release().perform();

		MultiTouchAction mta = new MultiTouchAction(driver);
		mta.add(action1).add(action2);
		mta.perform();

		/*
		 * TouchAction actionOne = new TouchAction(driver); actionOne.press(10, 10);
		 * actionOne.moveTo(10, 100); actionOne.release(); TouchAction actionTwo = new
		 * TouchAction(driver); actionTwo.press(20, 20); actionTwo.moveTo(20, 200);
		 * actionTwo.release(); MultiTouchAction action = new MultiTouchAction(driver);
		 * action.add(actionOne); action.add(actionTwo); action.perform();
		 */

	}

	public void rectangleFileInfoLinkSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnRectangleInfolink, 20);
		btnRectangleInfolink.click();
		Log.message("Rectangle Info Link button clicked");
		ARCFNAAppUtils.waitForElement(driver, btnRectangleInfolinkSquare, 20);
		btnRectangleInfolinkSquare.click();
		Log.message("Rectangle Info Link Square button clicked");
		Log.message("Rectangle info link selected and locked");
		// Thread.sleep(1000);
	}

	public void rectangleFileInfoLinkSelectionLock() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnRectangleInfolink, 20);
		btnRectangleInfolink.click();
		Log.message("Rectangle Info Link button clicked");
		ARCFNAAppUtils.waitForElement(driver, btnRectangleInfolinkSquare, 20);
		btnRectangleInfolinkSquare.click();
		btnRectangleInfolink.click();
		ARCFNAAppUtils.waitTill(5000);
		// btnRectangleInfolinkSquare.click();
		Log.message("Rectangle Info Link Square button clicked");
		// btnRectangleInfolink.click();

		// ARCFNAAppUtils.waitForElement(driver, btnRectangleInfolinkSquare, 20);
		// btnRectangleInfolinkSquare.click();
		Log.message("Rectangle info link selected and locked");
		// Thread.sleep(1000);
	}

	public void circleFileInfoLinkSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnRectangleInfolink, 20);
		btnRectangleInfolink.click();
		Log.message("Rectangle Info Link button clicked");
		ARCFNAAppUtils.waitForElement(driver, btnCircle, 20);
		btnCircle.click();
		Log.message("Circle Info Link button clicked");
	}

	public void cloudFileInfoLinkSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnRectangleInfolink, 20);
		btnRectangleInfolink.click();
		Log.message("Rectangle Info Link button clicked");
		ARCFNAAppUtils.waitForElement(driver, btnCloudInfolink, 20);
		btnCloudInfolink.click();
		Log.message("Circle Info Link button clicked");
	}

	public void markupAnnotationDrawing() throws FindFailed, InterruptedException {

		TouchAction action = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 30);
		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			action.longPress(481, 407).moveTo(100, 100).release().perform();
			Log.message("Annotation drawn using Square infolink");
		}

		// Log.message("Input canvas not displayed");
		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		Log.message("Annotation Save button clicked");
		Thread.sleep(2000);
		driver.switchTo().alert();
		txtBoxMarkupSaving.sendKeys("Annotation");
		Log.message("Mark up name has been entered");
		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
	}

	public boolean verifySquareMarkupAnnotationDrawingOffline() throws FindFailed, InterruptedException {
		TouchAction action = new TouchAction(driver);

		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			action.longPress(481, 407).moveTo(100, 100).release().perform();
			Log.message("Annotation drawn using Square infolink");
		}
		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		Log.message("Annotation Save button clicked");
		Thread.sleep(1000);
		String blockActualMessage = driver.switchTo().alert().getText();
		String blockExpectedMessage = "Network Error. Please check your connection and try again.";
		if (blockActualMessage.equals(blockExpectedMessage)) {
			driver.switchTo().alert().accept();
			return true;
		} else {
			driver.switchTo().alert().accept();
			return false;
		}
	}

	public void onlyMarkupDrawing() {
		TouchAction action = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 30);
		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			action.longPress(481, 407).moveTo(100, 100).release().perform();
			Log.message("Annotation drawn using Square infolink");
		}
	}

	@SuppressWarnings("unused")
	public boolean checkingSavedMarkup() throws InterruptedException, IOException {
		Log.message("****1****");
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Log.message("****2****");
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingMarkup.png"));
		Log.message("****3****");
		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingMarkup.png");
		Log.message("****4****");
		Log.message("Screenshot taken of saved markup");
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();
		TouchAction a1 = new TouchAction(driver);
		a1.press(100, 206).release().perform();
		btnViewDropDown.click();
		a1.press(100, 206).release().perform();
		Log.message("Saved markup has been opned using viewer drop down");
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingMarkup.png"));
		ARCFNAAppUtils.cropImage(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingMarkup.png");
		Log.message("Screenshot taken of opened markup");
		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingMarkup.png"));
		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingMarkup.png"));
		Log.message("Checking the height and width of both the images are same?");
		Log.message("Width of image before saving:" + " " + Integer.toString(imgA.getWidth()));
		Log.message("Width of image after saving:" + " " + Integer.toString(imgB.getWidth()));
		Log.message("Height of image before saving:" + " " + Integer.toString(imgA.getHeight()));
		Log.message("Height of image after saving:" + " " + Integer.toString(imgB.getHeight()));

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	@SuppressWarnings("unused")
	public boolean checkingSavedMarkupZoom() throws InterruptedException, IOException {
		ARCFNAAppUtils.waitTill(3000);
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingMarkup.png"));
		ARCFNAAppUtils.cropImageZoom(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingMarkup.png");
		Log.message("Screenshot taken of saved markup");
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();
		TouchAction a1 = new TouchAction(driver);
		a1.press(100, 206).release().perform();
		btnViewDropDown.click();
		a1.press(100, 206).release().perform();
		Log.message("Saved markup has been opned using viewer drop down");
		ARCFNAAppUtils.waitTill(2000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingMarkup.png"));
		ARCFNAAppUtils.cropImageZoom(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingMarkup.png");
		Log.message("Screenshot taken of opened markup");
		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingMarkup.png"));
		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingMarkup.png"));
		Log.message("Checking the height and width of both the images are same?");
		Log.message("Width of image before saving:" + " " + Integer.toString(imgA.getWidth()));
		Log.message("Width of image after saving:" + " " + Integer.toString(imgB.getWidth()));
		Log.message("Height of image before saving:" + " " + Integer.toString(imgA.getHeight()));
		Log.message("Height of image after saving:" + " " + Integer.toString(imgB.getHeight()));

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}

		return true;

	}

	@SuppressWarnings("unused")
	public boolean checkingSavedCircleMarkup() throws InterruptedException, IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingCircle.png"));
		// Thread.sleep(2000);
		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingCircle.png");
		Log.message("Screenshot taken of saved markup");
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();
		ARCFNAAppUtils.waitForElement(driver, selectSavedMarkup, 10);
		selectSavedMarkup.click();
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 5);
		btnViewDropDown.click();
		ARCFNAAppUtils.waitForElement(driver, selectSavedMarkup, 5);
		selectSavedMarkup.click();
		Log.message("Saved markup has been opned using viewer drop down");
		// Thread.sleep(2000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingCircle.png"));
		// Thread.sleep(2000);
		ARCFNAAppUtils.cropImage(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingCircle.png");
		Log.message("Screenshot taken of opened markup");
		// btnViewDropDown.click();
		// selectSavedMarkup.click();
		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingCircle.png"));
		// Thread.sleep(2000);
		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingCircle.png"));
		// Thread.sleep(2000);
		Log.message("Checking the height and width of both the images are same?");
		Log.message("Width of image before saving:" + " " + Integer.toString(imgA.getWidth()));
		Log.message("Width of image after saving:" + " " + Integer.toString(imgB.getWidth()));
		Log.message("Height of image before saving:" + " " + Integer.toString(imgA.getHeight()));
		Log.message("Height of image after saving:" + " " + Integer.toString(imgB.getHeight()));

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	@SuppressWarnings("unused")
	public boolean checkingSavedCloudMarkup() throws InterruptedException, IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingCloud.png"));

		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingCloud.png");
		Log.message("Screenshot taken of saved markup");
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();

		selectSavedMarkup.click();

		btnViewDropDown.click();
		selectSavedMarkup.click();
		Log.message("Saved markup has been opned using viewer drop down");

		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingCloud.png"));

		ARCFNAAppUtils.cropImage(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingCloud.png");
		Log.message("Screenshot taken of opened markup");

		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingCloud.png"));

		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingCloud.png"));

		Log.message("Checking the height and width of both the images are same?");
		Log.message("Width of image before saving:" + " " + Integer.toString(imgA.getWidth()));
		Log.message("Width of image after saving:" + " " + Integer.toString(imgB.getWidth()));
		Log.message("Height of image before saving:" + " " + Integer.toString(imgA.getHeight()));
		Log.message("Height of image after saving:" + " " + Integer.toString(imgB.getHeight()));

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public void rotationToolbarSelection() {
		ARCFNAAppUtils.waitForElement(driver, rotationToolbar, 50);
		rotationToolbar.click();
		Log.message("Rotation Tool bar button has been clicked");
	}

	@SuppressWarnings("rawtypes")
	public void deleteMarkups() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();
		Log.message("Drop Down button has been clicked");

		ARCFNAAppUtils.waitForElement(driver, selectSavedMarkup, 50);
		int noOfMarkup = selectSavedMarkup.findElements(By.name("deselect mark")).size();
		Log.message("Markups count" + " " + noOfMarkup);
		Log.message("Deleting all the existing markups");

		if (noOfMarkup > 0) {
			for (int i = 0; i < noOfMarkup; i++) {

				((MobileDriver) driver).swipe(330, 206, 118, 206, 2000);
				Log.message("Delete button has been clicked");
				ARCFNAAppUtils.waitTill(3000);
				driver.switchTo().alert().accept();
				Log.message("Yes button has been clicked");
				ARCFNAAppUtils.waitTill(3000);
			}
		}
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 10);
		btnViewDropDown.click();
		Log.message("All existing markups are deleted");

	}

	public boolean deleteMarkupsOffline() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();
		Log.message("Drop Down button has been clicked");
		ARCFNAAppUtils.waitForElement(driver, selectSavedMarkup, 50);
		int noOfMarkup = selectSavedMarkup.findElements(By.name("deselect mark")).size();
		Log.message("Markups count" + " " + noOfMarkup);
		Log.message("Deleting all the existing markups");

		if (noOfMarkup > 0) {
			for (int i = 0; i < noOfMarkup; i++) {
				((MobileDriver) driver).swipe(330, 206, 118, 206, 2000);

				ARCFNAAppUtils.waitTill(2000);
				driver.switchTo().alert().accept();
				String actualAlertMsg = driver.switchTo().alert().getText();
				String expectedAlertMsg = "Internet not available. Please check your network settings.";
				driver.switchTo().alert().accept();

				if (actualAlertMsg.equals(expectedAlertMsg)) {
					return true;
				} else {
					return false;
				}

			}
		}
		return true;
	}

	public void lineMarkupSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnLineMarkup, 20);
		btnLineMarkup.click();
		Log.message("Line markup button has been clicked");

		btnLineMarkupArrow.click();

		Log.message("Arrow line markup selected");

	}

	public void lineMarkuplineSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnLineMarkup, 20);
		btnLineMarkup.click();
		Log.message("Line markup button has been clicked");
		btnLineMarkupline.click();
		Log.message("Line markup Line selected");
	}

	public void lineMarkupAnnotationDrawing() throws FindFailed, InterruptedException {
		TouchAction action = new TouchAction(driver);

		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");

			action.longPress(481, 407).moveTo(510, 455).release().perform();
			Log.message("Annotation drawn using Square infolink");

		}

		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		Log.message("Annotation Save button clicked");
		driver.switchTo().alert();

		txtBoxMarkupSaving.sendKeys("ArrowLineAnnotation");

		driver.switchTo().alert().accept();

		Log.message("Annotation drawn and saved");

		action.release();
	}

	public boolean checkingSavedLineMarkup() throws InterruptedException, IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingLine.png"));

		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingLine.png");
		Log.message("Screenshot taken of saved markup");
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();

		selectSavedMarkup.click();

		btnViewDropDown.click();
		selectSavedMarkup.click();
		Log.message("Saved markup has been opned using viewer drop down");
		Thread.sleep(2000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingLine.png"));

		ARCFNAAppUtils.cropImage(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingLine.png");
		Log.message("Screenshot taken of opened markup");

		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingLine.png"));

		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingLine.png"));

		Log.message("Checking the height and width of both the images are same?");
		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public boolean checkingSavedLineMarkupLine() throws InterruptedException, IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingLine1.png"));
		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingLine1.png");
		Log.message("Screenshot taken of saved markup");
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();
		selectSavedMarkup.click();
		btnViewDropDown.click();
		selectSavedMarkup.click();
		Log.message("Saved markup has been opned using viewer drop down");
		Thread.sleep(2000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingLine1.png"));
		ARCFNAAppUtils.cropImage(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingLine1.png");
		Log.message("Screenshot taken of opened markup");
		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingLine1.png"));
		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingLine1.png"));
		Log.message("Checking the height and width of both the images are same?");
		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public void textMarkupSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnTextMarkup, 20);
		btnTextMarkup.click();
		// ARCFNAAppUtils.waitForElement(driver, btnTextMarkupCallout, 20);
		btnTextMarkupCallout.click();
		// ARCFNAAppUtils.waitForElement(driver, btnRectangleInfolinkSquare, 20);
		Log.message("Callout text markup selected");
		// Thread.sleep(2000);
	}

	public void textInfoMarkupSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnTextMarkup, 20);
		btnTextMarkup.click();
		Log.message("Text markup selected");
		ARCFNAAppUtils.waitForElement(driver, btnTextInfoSub, 20);
		btnTextInfoSub.click();
		Log.message("Text Markup text selected");
	}

	public void textInfoNoteMarkupSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnTextMarkup, 20);
		btnTextMarkup.click();
		Log.message("Text markup selected");
		ARCFNAAppUtils.waitForElement(driver, btnTextNoteInfoLink, 20);
		btnTextNoteInfoLink.click();
		Log.message("Text Markup Note selected");
	}

	/* Do not delete */
	public void textMarkupAnnotationDrawing() throws FindFailed, InterruptedException {

		// Screen screen = new Screen();
		TouchAction action = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 30);
		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			// action.press(276, 577).moveTo(100,100).release().perform();
			action.longPress(481, 407).moveTo(510, 455).release().perform();
			Log.message("Annotation drawn using Square infolink");
			// Thread.sleep(1000);

		}

		ARCFNAAppUtils.waitForElement(driver, txtAreaCalloutMarkup, 20);
		txtAreaCalloutMarkup.sendKeys("This is a test message");
		// ((MobileElement) txtAreaCalloutMarkup).setValue("This is a test message");
		btnDoneCallout.click();
		// ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		// Thread.sleep(2000);
		driver.switchTo().alert();
		// Thread.sleep(2000);
		txtBoxMarkupSaving.sendKeys("TextAnnotation");
		// ((MobileElement) txtBoxMarkupSaving).setValue("CalloutTextAnnotation");
		driver.switchTo().alert().accept();
		// ARCFNAAppUtils.waitForElement(driver, btnSaveMarkupName, 30);
		// btnSaveMarkupName.click();
		// alert()
		Log.message("Annotation drawn and saved");
		// Thread.sleep(2000);
		action.release();
	}

	public void textMarkupNoteAnnotationDrawing() throws FindFailed, InterruptedException {
		ARCFNAAppUtils.waitTill(3000);
		// Screen screen = new Screen();
		TouchAction action = new TouchAction(driver);

		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			// action.press(276, 577).moveTo(100,100).release().perform();
			action.longPress(481, 407).moveTo(510, 455).release().perform();
			Log.message("Annotation drawn using Square infolink");
			// Thread.sleep(1000);

		}

		ARCFNAAppUtils.waitForElement(driver, txtViewNote, 30);
		txtViewNote.sendKeys("This is a test message");
		ARCFNAAppUtils.waitForElement(driver, btnTextNoteDone, 20);
		btnTextNoteDone.click();

		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		driver.switchTo().alert();
		txtBoxMarkupSaving.sendKeys("TextAnnotation");
		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
	}

	public void textMarkupNoteAnnotationDrawing1() throws FindFailed, InterruptedException {
		ARCFNAAppUtils.waitTill(3000);
		// Screen screen = new Screen();
		TouchAction action = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 30);
		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			// action.press(276, 577).moveTo(100,100).release().perform();
			action.longPress(481, 407).moveTo(510, 455).release().perform();
			Log.message("Annotation drawn using Square infolink");
			// Thread.sleep(1000);

		}

		ARCFNAAppUtils.waitForElement(driver, txtViewNote3, 30);
		// txtViewNote3.sendKeys("This is a test message");
		driver.getKeyboard().sendKeys("This is a test message");
		ARCFNAAppUtils.waitForElement(driver, btnTextNoteDone, 20);
		btnTextNoteDone.click();

		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		driver.switchTo().alert();
		txtBoxMarkupSaving.sendKeys("TextAnnotation");
		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
	}

	public boolean checkingSavedTextMarkup() throws InterruptedException, IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingText.png"));
		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingText.png");
		Log.message("Screenshot taken of saved markup");
		btnViewDropDown.click();
		selectSavedMarkup.click();
		btnViewDropDown.click();
		selectSavedMarkup.click();
		Log.message("Saved markup has been opned using viewer drop down");
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingText.png"));
		ARCFNAAppUtils.cropImage(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingText.png");
		Log.message("Screenshot taken of opened markup");
		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingText.png"));
		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingText.png"));
		Log.message("Checking the height and width of both the images are same?");
		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public boolean checkingSavedTextMarkupText() throws InterruptedException, IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingText1.png"));
		// Thread.sleep(2000);
		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingText1.png");
		Log.message("Screenshot taken of saved markup");
		// ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();
		// action.tap(btnViewDropDown).release().perform();
		// Thread.sleep(2000);
		// ARCFNAAppUtils.waitForElement(driver, selectSavedMarkup, 50);
		selectSavedMarkup.click();
		// Thread.sleep(2000);
		btnViewDropDown.click();
		selectSavedMarkup.click();
		Log.message("Saved markup has been opned using viewer drop down");
		// Thread.sleep(2000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingText1.png"));
		// Thread.sleep(2000);
		ARCFNAAppUtils.cropImage(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingText1.png");
		Log.message("Screenshot taken of opened markup");
		// btnViewDropDown.click();
		// selectSavedMarkup.click();
		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingText1.png"));
		// Thread.sleep(2000);
		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingText1.png"));
		// Thread.sleep(2000);
		Log.message("Checking the height and width of both the images are same?");
		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public boolean checkingSavedTextMarkupNote() throws InterruptedException, IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingTextNote.png"));
		// Thread.sleep(2000);
		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingNote.png");
		Log.message("Screenshot taken of saved markup");
		// ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();
		// action.tap(btnViewDropDown).release().perform();
		// Thread.sleep(2000);
		// ARCFNAAppUtils.waitForElement(driver, selectSavedMarkup, 50);
		selectSavedMarkup.click();
		// Thread.sleep(2000);
		btnViewDropDown.click();
		selectSavedMarkup.click();
		Log.message("Saved markup has been opned using viewer drop down");
		// Thread.sleep(2000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingText1.png"));
		// Thread.sleep(2000);
		ARCFNAAppUtils.cropImage(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingNote.png");
		Log.message("Screenshot taken of opened markup");
		// btnViewDropDown.click();
		// selectSavedMarkup.click();
		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingNote.png"));
		// Thread.sleep(2000);
		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingNote.png"));
		// Thread.sleep(2000);
		Log.message("Checking the height and width of both the images are same?");
		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public void freehandMarkupSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnMarkerFree, 20);
		btnMarkerFree.click();
		// ARCFNAAppUtils.waitForElement(driver, btnMarkerFreehand, 20);
		btnMarkerFreehand.click();
		// ARCFNAAppUtils.waitForElement(driver, btnRectangleInfolinkSquare, 20);
		Log.message("Marker markup selected");
		// Thread.sleep(2000);
	}

	public void markerMarkupSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnMarkerFree, 20);
		btnMarkerFree.click();
		btnMarkerMarkupSub.click();
		Log.message("Marker markup selected");
	}

	public void markerMarkupAnnotationDrawing() throws FindFailed, InterruptedException {

		// Screen screen = new Screen();
		TouchAction action = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 30);
		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			// action.press(276, 577).moveTo(100,100).release().perform();
			action.longPress(481, 430).moveTo(510, 455).release().perform();
			Log.message("Annotation drawn using Square infolink");
			// Thread.sleep(2000);

		}

		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		// Thread.sleep(2000);
		driver.switchTo().alert();
		// Thread.sleep(2000);
		txtBoxMarkupSaving.sendKeys("MarkerAnnotation");
		// ((MobileElement) txtBoxMarkupSaving).setValue("MarkerAnnotation");
		driver.switchTo().alert().accept();
		// ARCFNAAppUtils.waitForElement(driver, btnSaveMarkupName, 30);
		// btnSaveMarkupName.click();
		// alert()
		Log.message("Annotation drawn and saved");
		// Thread.sleep(2000);
		action.release();
	}

	public boolean checkingSavedMarkerMarkup() throws InterruptedException, IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingMarker.png"));
		Log.message("Screenshot taken of saved markup");
		// ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();
		// action.tap(btnViewDropDown).release().perform();
		// Thread.sleep(2000);
		// ARCFNAAppUtils.waitForElement(driver, selectSavedMarkup, 50);
		selectSavedMarkup.click();
		// Thread.sleep(2000);
		btnViewDropDown.click();
		selectSavedMarkup.click();
		Log.message("Saved markup has been opned using viewer drop down");
		// Thread.sleep(2000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingMarker.png"));
		// Thread.sleep(2000);
		Log.message("Screenshot taken of opened markup");
		// btnViewDropDown.click();
		// selectSavedMarkup.click();
		BufferedImage imgA = ImageIO.read(new File("./InitialScreenShots/BeforeSavingMarker.png"));
		// Thread.sleep(2000);
		BufferedImage imgB = ImageIO.read(new File("./InitialScreenShots/AfterSavingMarker.png"));
		// Thread.sleep(2000);
		Log.message("Checking the height and width of both the images are same?");
		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public boolean checkingSavedHyperlinkMarkupSub() throws InterruptedException, IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingMarker1.png"));

		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingMarker1.png");
		Log.message("Screenshot taken of saved markup");

		btnViewDropDown.click();

		ARCFNAAppUtils.waitForElement(driver, selectSavedMarkup, 50);
		selectSavedMarkup.click();

		btnViewDropDown.click();
		selectSavedMarkup.click();
		Log.message("Saved markup has been opned using viewer drop down");

		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingMarker1.png"));

		ARCFNAAppUtils.cropImage(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingMarker1.png");
		Log.message("Screenshot taken of opened markup");

		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingMarker1.png"));

		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingMarker1.png"));

		Log.message("Checking the height and width of both the images are same?");
		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public void hyperlinkMarkupSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkMarkup, 30);
		btnHyperlinkMarkup.click();
		// ARCFNAAppUtils.waitForElement(driver, btnHyperlinkMarkupSquare, 20);
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkMarkupSquare, 20);
		btnHyperlinkMarkupSquare.click();
		// ARCFNAAppUtils.waitForElement(driver, btnRectangleInfolinkSquare, 20);
		Log.message("Hyperlink markup selected");
		// Thread.sleep(2000);
	}

	public void hyperlinkCircleMarkupSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkMarkup, 20);
		btnHyperlinkMarkup.click();
		btnCirclehyperlink.click();
		Log.message("Hyperlink Circle markup selected");
	}

	public void hyperlinkCloudMarkupSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkMarkup, 20);
		btnHyperlinkMarkup.click();
		btnCloudhyperlink.click();
		Log.message("Hyperlink Circle markup selected");
	}

	public void hyperlinkMarkupAnnotationDrawing() throws FindFailed, InterruptedException {

		// Screen screen = new Screen();
		TouchAction action = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 30);
		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			// action.press(481, 407).moveTo(50,50).release().perform();
			action.longPress(481, 407).moveTo(510, 455).release().perform();
			Log.message("Annotation drawn using Square infolink");
			// Thread.sleep(2000);

		}
		action.release();
		Log.message("Markup annotation drawing successful");
	}

	public void reHyperlinkMarkupAnnotationDrawing() throws FindFailed, InterruptedException {

		// Screen screen = new Screen();
		TouchAction action1 = new TouchAction(driver);

		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			// action.press(481, 407).moveTo(50,50).release().perform();
			action1.longPress(510, 407).moveTo(530, 455).release().perform();
			Log.message("Annotation drawn using Square infolink");
			// Thread.sleep(2000);

		}
		action1.release();
	}

	public void clickExternalLink() {
		ARCFNAAppUtils.waitForElement(driver, btnExternalLink, 20);
		btnExternalLink.click();
	}

	public void linkPaste() {
		ARCFNAAppUtils.waitForElement(driver, txtHyperlinkTextView, 20);
		txtHyperlinkTextView.sendKeys("https://www.google.com");
	}

	public void clickLinkbutton() {
		ARCFNAAppUtils.waitForElement(driver, btnLink, 20);
		btnLink.click();
	}

	public void clickLinkbuttonExternal() {
		ARCFNAAppUtils.waitForElement(driver, btnLink1, 20);
		btnLink1.click();
	}

	public void savingAnnotation() {
		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		btnAnnotationSave.click();
		driver.switchTo().alert();
		driver.getKeyboard().sendKeys("Sample Annotation");
		driver.switchTo().alert().accept();
	}

	public void hideAndVisibleAnnotations() {

	}

	public void hyperlinkMarkupAnnotationDrawing1() throws FindFailed, InterruptedException {

		// Screen screen = new Screen();
		TouchAction action = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 30);
		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			action.longPress(500, 407).moveTo(510, 455).release().perform();
			Log.message("Annotation drawn using Square infolink");
			// Thread.sleep(2000);

		}
		action.release();
	}

	public void hyperLinkFileSelectionSameFolder() throws InterruptedException {
		ARCFNAAppUtils.waitTill(3000);
		Log.message("Search field is found");
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 30);
		Log.message("Search field is found");
		// TouchAction a4 = new TouchAction(driver);
		// a4.longPress(btnHyperlinkFileSearch).release().perform();
		// btnHyperlinkFileSearchWithOutCache.click();
		driver.getKeyboard().sendKeys("Automation_Testing1");
		ARCFNAAppUtils.waitForElement(driver, searchedFolder1, 30);
		searchedFolder1.click();
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearchWithOutCache, 20);

		btnHyperlinkFileSearch.clear();

		driver.getKeyboard().sendKeys("RotatePDFPart-1");
		ARCFNAAppUtils.waitForElement(driver, searchedFile, 20);
		searchedFile.click();

		ARCFNAAppUtils.waitForElement(driver, btnLink1, 20);
		btnLink1.click();

		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		btnAnnotationSave.click();
		driver.switchTo().alert();
		// txtBoxMarkupSaving.sendKeys("HyperlinkAnnotation");
		driver.getKeyboard().sendKeys("HyperlinkAnnotation");
		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
	}

	public void hyperLinkFileSelectionDiffFolder() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 30);
		// btnHyperlinkFileSearch.sendKeys("Automation_Testing2");
		btnHyperlinkFileSearch.click();
		driver.getKeyboard().sendKeys("Automation_Testing2");
		ARCFNAAppUtils.waitForElement(driver, searchedFolder2, 30);
		searchedFolder2.click();
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 20);
		btnHyperlinkFileSearch.clear();
		driver.getKeyboard().sendKeys("Test OCR 3.pdf");

		ARCFNAAppUtils.waitForElement(driver, searchedFile1, 20);
		searchedFile1.click();

		// searchedFile1.click();
		ARCFNAAppUtils.waitForElement(driver, btnLink, 20);
		btnLink.click();

		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		btnAnnotationSave.click();

		driver.switchTo().alert();
		driver.getKeyboard().sendKeys("HyperlinkAnnotation1");

		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
	}

	public void hyperLinkFileSelectionDiffFolderSameFileDiffVersion() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 20);
		btnHyperlinkFileSearch.click();

		driver.getKeyboard().sendKeys("Automation_Testing2");
		ARCFNAAppUtils.waitForElement(driver, searchedFolder2, 20);
		searchedFolder2.click();

		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 20);
		btnHyperlinkFileSearch.clear();
		driver.getKeyboard().sendKeys("Release PDF.PDF");

		ARCFNAAppUtils.waitForElement(driver, searchedFile2, 30);
		searchedFile2.click();

		ARCFNAAppUtils.waitForElement(driver, btnLink1, 20);
		btnLink1.click();

		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		btnAnnotationSave.click();

		driver.switchTo().alert();
		driver.getKeyboard().sendKeys("HyperlinkAnnotation2");

		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
	}

	public boolean checkingSavedHyperlinkMarkup() throws InterruptedException, IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingMarker.png"));

		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingMarker.png");
		Log.message("Screenshot taken of saved markup");

		btnViewDropDown.click();

		ARCFNAAppUtils.waitForElement(driver, selectSavedMarkup, 50);
		selectSavedMarkup.click();

		btnViewDropDown.click();
		selectSavedMarkup.click();
		Log.message("Saved markup has been opned using viewer drop down");

		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingMarker.png"));

		ARCFNAAppUtils.cropImage(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingMarker.png");
		Log.message("Screenshot taken of opened markup");

		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingMarker.png"));

		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingMarker.png"));

		Log.message("Checking the height and width of both the images are same?");
		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public boolean verifyHyperlinkFunctionlitySameFolderDiffFile() throws InterruptedException {

		TouchAction action1 = new TouchAction(driver);
		action1.press(481, 407).release().perform();
		Log.message("Hyperlink clicked");

		ARCFNAAppUtils.waitForElement(driver, fileNameThumbnail, 60);

		fileNameThumbnail.click();

		ARCFNAAppUtils.waitForElement(driver, hyperLinkfileName1, 40);
		if (hyperLinkfileName1.isDisplayed()) {
			Log.message("The hyperlinked file opened successfully");
			return true;

		} else {
			Log.message("The hyperlinked file not opened successfully");
			return false;
		}

	}

	public boolean verifyHyperlinkFunctionlityDiffFolderDiffFile() throws InterruptedException {
		ARCFNAAppUtils.waitTill(3000);
		TouchAction action1 = new TouchAction(driver);
		action1.press(481, 407).release().perform();
		Log.message("Hyperlink clicked");
		ARCFNAAppUtils.waitForElement(driver, fileNameThumbnail, 80);
		fileNameThumbnail.click();
		ARCFNAAppUtils.waitForElement(driver, hyperLinkfileName2, 40);
		if (hyperLinkfileName2.isDisplayed()) {
			Log.message("The hyperlinked file opened successfully");
			return true;

		} else {
			Log.message("The hyperlinked file not opened successfully");
			return false;
		}

	}

	public boolean verifyHyperlinkFunctionlityDiffFolderSameFileDiffVersion() throws InterruptedException {
		ARCFNAAppUtils.waitTill(3000);
		TouchAction action1 = new TouchAction(driver);
		action1.press(481, 407).release().perform();
		Log.message("Hyperlink clicked");
		ARCFNAAppUtils.waitForElement(driver, fileNameThumbnail, 60);
		fileNameThumbnail.click();
		ARCFNAAppUtils.waitForElement(driver, hyperLinkfileName5, 40);
		if (hyperLinkfileName5.isDisplayed()) {
			Log.message("The hyperlinked file opened successfully");
			return true;
		} else {
			Log.message("The hyperlinked file not opened successfully");
			return false;
		}

	}

	public boolean verifyHyperlinkFunctionlityDiffFolderPrnttoChldDiffFile() throws InterruptedException {
		ARCFNAAppUtils.waitTill(3000);
		TouchAction action1 = new TouchAction(driver);
		action1.longPress(481, 407).release().perform();
		Log.message("Hyperlink clicked");
		ARCFNAAppUtils.waitForElement(driver, fileNameThumbnail, 60);
		fileNameThumbnail.click();
		ARCFNAAppUtils.waitForElement(driver, hyperLinkfileName4, 40);
		if (hyperLinkfileName4.isDisplayed()) {
			Log.message("The hyperlinked file opened successfully");
			return true;

		} else {
			Log.message("The hyperlinked file not opened successfully");
			return false;
		}

	}

	public void calibratorkMarkupSelection() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorWithOutCash, 30);
		btnCalibratorWithOutCash.click();
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorSubOptionWithOutCash, 30);
		// JavascriptExecutor executor = (JavascriptExecutor)driver;
		// executor.executeScript("arguments[0].click();", btnCalibratorSubOption);
		btnCalibratorSubOptionWithOutCash.click();
		// ARCFNAAppUtils.waitForElement(driver, btnRectangleInfolinkSquare, 20);
		Log.message("Calibrator markup selected");
		Thread.sleep(1000);
	}

	public void reCalibratorkMarkupSelection() throws InterruptedException {
		/*
		 * WebElement btnCalibrator =
		 * driver.findElement(By.id("Measurement Calibrator"));
		 * ARCFNAAppUtils.waitForElement(driver, btnCalibrator, 30);
		 * btnCalibrator.click(); WebElement btnSubCalibrator =
		 * driver.findElement(By.id("calibration"));
		 * ARCFNAAppUtils.waitForElement(driver, btnSubCalibrator, 30);
		 * btnSubCalibrator.click();
		 */

		ARCFNAAppUtils.waitForElement(driver, btnCalibratorWithOutCash, 30);
		btnCalibratorWithOutCash.click();
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorSubOptionWithOutCash, 30);
		btnCalibratorSubOptionWithOutCash.click();
		Log.message("Calibrator markup selected");
		Thread.sleep(1000);
	}

	public void clickOnCalibrateRuler() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnCalibrator, 20);
		btnCalibrator.click();
	}

	public void calibratorDataFillupFeet() throws InterruptedException {
		// ARCFNAAppUtils.waitForElement(driver, txtLengthCalibrator, 20);

		Log.message("******!!!!! Started.");

		ARCFNAAppUtils.waitForElement(driver, txtFeet, 20);
		txtFeet.sendKeys("5");
		Log.message("******!!!!! Started.");
		ARCFNAAppUtils.waitForElement(driver, textInches, 20);
		textInches.sendKeys("2");
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorDone, 30);
		btnCalibratorDone.click();
		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		Thread.sleep(1000);
		driver.switchTo().alert();
		Thread.sleep(1000);
		txtBoxMarkupSaving.sendKeys("CalibratorAnnotation");
		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
	}

	public void reCalibratorDataFillupFeet() throws InterruptedException {
		// ARCFNAAppUtils.waitForElement(driver, txtLengthCalibrator, 20);

		Log.message("******!!!!! Started.");

		ARCFNAAppUtils.waitForElement(driver, txtFeetWithOutCache, 20);
		txtFeetWithOutCache.sendKeys("5");
		ARCFNAAppUtils.waitForElement(driver, textInchesWithOutCache, 20);
		textInchesWithOutCache.sendKeys("2");
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorDone, 30);
		btnCalibratorDone.click();
		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		Thread.sleep(1000);
		driver.switchTo().alert();
		Thread.sleep(1000);
		txtBoxMarkupSaving.sendKeys("CalibratorAnnotation1");
		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
	}

	public void calibratorDataFillupMeter() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, tabCalibratorMetric, 20);
		tabCalibratorMetric.click();
		ARCFNAAppUtils.waitForElement(driver, txtCalibratorMeter, 20);
		txtCalibratorMeter.sendKeys("5");
		ARCFNAAppUtils.waitForElement(driver, txtCalibratorCentiMeter, 20);
		txtCalibratorCentiMeter.sendKeys("2");
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorDone, 30);
		btnCalibratorDone.click();
		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		Thread.sleep(1000);
		driver.switchTo().alert();
		Thread.sleep(1000);
		txtBoxMarkupSaving.sendKeys("CalibratorAnnotation1");
		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
	}

	public void calibratorDataFillupWithoutSave() throws InterruptedException {
		// ARCFNAAppUtils.waitForElement(driver, txtLengthCalibrator, 20);
		ARCFNAAppUtils.waitForElement(driver, txtFeet, 20);
		txtFeet.sendKeys("5");
		ARCFNAAppUtils.waitForElement(driver, textInches, 20);
		textInches.sendKeys("2");

		ARCFNAAppUtils.waitForElement(driver, btnCalibratorDone, 30);
		btnCalibratorDone.click();
	}

	public void calibratorDataFillupFeet1() throws InterruptedException {
		// ARCFNAAppUtils.waitForElement(driver, txtLengthCalibrator, 20);
		ARCFNAAppUtils.waitForElement(driver, txtFeet, 20);
		txtFeet.sendKeys("6");
		ARCFNAAppUtils.waitForElement(driver, textInches, 20);
		textInches.sendKeys("4");

		ARCFNAAppUtils.waitForElement(driver, btnCalibratorDone, 30);
		btnCalibratorDone.click();
		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		Thread.sleep(1000);
		driver.switchTo().alert();
		Thread.sleep(1000);
		txtBoxMarkupSaving.sendKeys("CalibratorAnnotation1");
		// ((MobileElement) txtBoxMarkupSaving).setValue("CalibratorAnnotation");
		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
		// Thread.sleep(2000);
	}

	public void reCalibratorDataFillupFeet1() throws InterruptedException {
		WebElement txtFeet1 = driver.findElement(By.id("PDFViewerCalibrationMessurementTextfieldSuper"));
		ARCFNAAppUtils.waitForElement(driver, txtFeet1, 20);
		txtFeet.sendKeys("6");
		WebElement textInches1 = driver.findElement(By.id("PDFViewerCalibrationMessurementTextfieldSub"));
		ARCFNAAppUtils.waitForElement(driver, textInches1, 20);
		textInches.sendKeys("4");

		ARCFNAAppUtils.waitForElement(driver, btnCalibratorDone, 30);
		btnCalibratorDone.click();
		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		Thread.sleep(1000);
		driver.switchTo().alert();
		Thread.sleep(1000);
		txtBoxMarkupSaving.sendKeys("CalibratorAnnotation1");
		// ((MobileElement) txtBoxMarkupSaving).setValue("CalibratorAnnotation");
		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
		// Thread.sleep(2000);
	}

	public void multiSelectButtonClick() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnMultiSelect, 30);
		btnMultiSelect.click();
	}

	public void selectAllButtonClick() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnSelectAll, 30);
		btnSelectAll.click();
	}

	public void removeAll() {
		ARCFNAAppUtils.waitForElement(driver, btnRemove, 20);
		btnRemove.click();
		ARCFNAAppUtils.waitTill(8000);
	}

	public void takeScreenShotBeforeDrawing() throws IOException {
		ARCFNAAppUtils.waitTill(6000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/BeforeMarkupDrawing.png"));
		ARCFNAAppUtils.waitTill(6000);
	}

	public void takeScreenShotAfterDelete() throws IOException {
		File srcFiler2 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler2, new File("./InitialScreenShots/AfterDeleteMarkup.png"));
		ARCFNAAppUtils.waitTill(3000);
	}

	public boolean imageMachingAfterDelete() throws IOException, InterruptedException {
		ARCFNAAppUtils.waitTill(3000);
		BufferedImage imgA = ImageIO.read(new File("./InitialScreenShots/BeforeMarkupDrawing.png"));
		// Thread.sleep(5000);
		BufferedImage imgB = ImageIO.read(new File("./InitialScreenShots/AfterDeleteMarkup.png"));
		// Thread.sleep(5000);
		Log.message("Checking the height and width of both the images are same?");
		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public void hyperLinkFileSelectionDiffFolderPrnttoChldDiffFile() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 20);
		btnHyperlinkFileSearch.click();
		driver.getKeyboard().sendKeys("Automation_Testing2");
		ARCFNAAppUtils.waitForElement(driver, searchedFolder2, 20);
		searchedFolder2.click();
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 20);
		btnHyperlinkFileSearch.clear();
		driver.getKeyboard().sendKeys("Automation_Testing2_1.0");
		ARCFNAAppUtils.waitForElement(driver, searchedFolder3, 20);
		searchedFolder3.click();
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 20);
		btnHyperlinkFileSearch.clear();
		driver.getKeyboard().sendKeys("Automation_Testing2_1.0.1");
		ARCFNAAppUtils.waitForElement(driver, searchedFolder4, 30);
		searchedFolder4.click();
		// TouchAction t1 = new TouchAction(driver);
		// t1.press(searchedFolder4).release().perform();
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 20);

		btnHyperlinkFileSearch.clear();
		driver.getKeyboard().sendKeys("A-101.pdf");
		ARCFNAAppUtils.waitForElement(driver, fileToBeLinked1, 20);
		fileToBeLinked1.click();

		ARCFNAAppUtils.waitForElement(driver, btnLink1, 20);
		btnLink1.click();
		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 30);
		btnAnnotationSave.click();
		btnAnnotationSave.click();

		driver.switchTo().alert();
		driver.getKeyboard().sendKeys("HyperlinkAnnotation3");
		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
	}

	public void openChildFolder() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnChildFolder1, 20);
		btnChildFolder1.click();
		ARCFNAAppUtils.waitForElement(driver, btnSkip, 20);
		btnSkip.click();
		ARCFNAAppUtils.waitForElement(driver, btnChildFolder2, 20);
		btnChildFolder2.click();
		ARCFNAAppUtils.waitForElement(driver, btnChildFolder3, 20);
		btnChildFolder3.click();
		// Thread.sleep(1000);
	}

	public void openChildFolder1() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnChildFolder1, 20);
		btnChildFolder1.click();
		ARCFNAAppUtils.waitForElement(driver, btnSkip, 20);
		btnSkip.click();
		// Thread.sleep(1000);
	}

	public void openChildFile() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnFile1, 20);
		btnFile1.click();
		Log.message("Desired file opened");
		ARCFNAAppUtils.waitForElement(driver, btnSkipFileOpening, 20);
		btnSkipFileOpening.click();
		Log.message("Skip button clicked");
		// Thread.sleep(1000);
	}

	public void hyperLinkFileSelectionChldtoPrnt() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 20);
		btnHyperlinkFileSearch.click();
		driver.getKeyboard().sendKeys("Automation_Testing1");
		ARCFNAAppUtils.waitForElement(driver, searchedFolder1, 20);
		searchedFolder1.click();
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 20);

		btnHyperlinkFileSearch.clear();
		driver.getKeyboard().sendKeys("Release PDF.PDF");
		ARCFNAAppUtils.waitForElement(driver, searchedFile2, 20);
		searchedFile2.click();

		ARCFNAAppUtils.waitForElement(driver, btnLink1, 20);
		btnLink1.click();

		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		btnAnnotationSave.click();

		driver.switchTo().alert();
		driver.getKeyboard().sendKeys("HyperlinkAnnotation4");

		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
		// Thread.sleep(2000);
	}

	public boolean verifyHyperlinkFunctionlityChldtoPrnt() throws InterruptedException {
		ARCFNAAppUtils.waitTill(4000);
		TouchAction action1 = new TouchAction(driver);
		action1.press(481, 407).release().perform();
		Log.message("Hyperlink clicked");
		ARCFNAAppUtils.waitForElement(driver, fileNameThumbnail, 60);
		fileNameThumbnail.click();
		ARCFNAAppUtils.waitForElement(driver, hyperLinkfileName3, 40);
		if (hyperLinkfileName3.isDisplayed()) {
			Log.message("The hyperlinked file opened successfully");
			return true;

		} else {
			Log.message("The hyperlinked file not opened successfully");
			return false;
		}

	}

	public void openChildFile1() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnFile2, 20);
		btnFile2.click();
		Log.message("Desired file opened");
		ARCFNAAppUtils.waitForElement(driver, btnSkipFileOpening, 20);
		btnSkipFileOpening.click();
		Log.message("Skip button clicked");
		// Thread.sleep(1000);
	}

	public void hyperLinkFileSelectionMultiDoc() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 20);
		btnHyperlinkFileSearch.click();
		driver.getKeyboard().sendKeys("Automation_Testing2");
		ARCFNAAppUtils.waitForElement(driver, searchedFolder2, 20);
		TouchAction action1 = new TouchAction(driver);
		action1.press(searchedFolder2).release().perform();
		// searchedFolder2.click();
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 20);

		btnHyperlinkFileSearch.clear();
		driver.getKeyboard().sendKeys("Test OCR 3.pdf");

		ARCFNAAppUtils.waitForElement(driver, searchedFile1, 40);
		searchedFile1.click();

		ARCFNAAppUtils.waitForElement(driver, pageNoTextBox, 30);
		pageNoTextBox.sendKeys("4");

		ARCFNAAppUtils.waitForElement(driver, btnLink, 20);
		btnLink.click();

		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		btnAnnotationSave.click();
		driver.switchTo().alert();
		txtBoxMarkupSaving.sendKeys("HyperlinkAnnotation5");

		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
	}

	public boolean verifyHyperlinkFunctionlityMultiDoc() throws InterruptedException {
		ARCFNAAppUtils.waitTill(4000);
//	Thread.sleep(2000);
		TouchAction action1 = new TouchAction(driver);
		// TouchAction action1 = new TouchAction((MobileDriver)
		// driver).press(el).waitAction(Duration.ofMillis(1000)).release().perform();
		action1.press(481, 407).release().perform();
		Log.message("Hyperlink clicked");
//	Thread.sleep(2000);
		// ARCFNAAppUtils.waitForElement(driver, hyperlinkJumpTofile, 40);
		// hyperlinkJumpTofile.click();
		// driver.switchTo().alert();
		// driver.switchTo().window("Jump to");
		ARCFNAAppUtils.waitForElement(driver, fileNameThumbnail, 60);
		// if(fileNameThumbnail.isDisplayed())
		// {
		// action1.tap(fileNameThumbnail);
		fileNameThumbnail.click();

		// Log.message("File is enabled");
		// Actions action = new Actions(driver);
		// action.moveToElement(fileNameThumbnail).click().perform();

		/*
		 * int length = fileNameThumbnail.getSize().getWidth();
		 * 
		 * int height = fileNameThumbnail.getSize().getHeight();
		 * 
		 * Point loc1 = fileNameThumbnail.getLocation(); int xcordi = loc1.getX();
		 * Log.message(Integer.toString(xcordi)); int ycordi = loc1.getY();
		 * Log.message(Integer.toString(ycordi));
		 * 
		 * int middleY = (int) (ycordi + height * 1.5);
		 * 
		 * //action2.press(xcordi, ycordi).release().perform(); TouchAction action2 =
		 * new TouchAction((MobileDriver<MobileElement>) driver);
		 * action2.press(fileNameThumbnail, length/2,
		 * middleY).waitAction(1000).release().perform();
		 */
		// }
		ARCFNAAppUtils.waitForElement(driver, hyperLinkfileName2, 20);
		if (hyperLinkfileName2.isDisplayed() && paginationArrowRight.isEnabled() == false) {
			Log.message("The hyperlinked file opened successfully");
			return true;

		} else {
			Log.message("The hyperlinked file not opened successfully");
			return false;
		}

	}

	@SuppressWarnings("unused")
	public boolean checkingSavedCalibratorMarkup() throws InterruptedException, IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingCalibrator.png"));
		// Thread.sleep(2000);
		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingCalibrator.png");
		Log.message("Screenshot taken of saved markup");
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();
		// action.tap(btnViewDropDown).release().perform();
//	Thread.sleep(2000);
		ARCFNAAppUtils.waitForElement(driver, selectSavedMarkup, 50);
		selectSavedMarkup.click();
//	Thread.sleep(2000);
		btnViewDropDown.click();
		selectSavedMarkup.click();
		Log.message("Saved markup has been opned using viewer drop down");
//	Thread.sleep(2000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingCalibrator.png"));
//	Thread.sleep(2000);
		ARCFNAAppUtils.cropImage(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingCalibrator.png");
		Log.message("Screenshot taken of opened markup");
		// btnViewDropDown.click();
		// selectSavedMarkup.click();
		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingCalibrator.png"));
		// Thread.sleep(2000);
		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingCalibrator.png"));
//	Thread.sleep(2000);
		Log.message("Checking the height and width of both the images are same?");
		Log.message("Width of image before saving:" + " " + Integer.toString(imgA.getWidth()));
		Log.message("Width of image after saving:" + " " + Integer.toString(imgB.getWidth()));
		Log.message("Height of image before saving:" + " " + Integer.toString(imgA.getHeight()));
		Log.message("Height of image after saving:" + " " + Integer.toString(imgB.getHeight()));

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public void clickThreedottedButton() {
		ARCFNAAppUtils.waitForElement(driver, btnThreeDotted, 50);
		btnThreeDotted.click();
	}

	public boolean verifyFileMoveToSameFldr() {
		ARCFNAAppUtils.waitForElement(driver, btnSelect, 20);
		btnSelect.click();
		ARCFNAAppUtils.waitForElement(driver, btnFile, 20);
		btnFile.click();
		ARCFNAAppUtils.waitForElement(driver, btnThreeDotted, 50);
		btnThreeDotted.click();
		ARCFNAAppUtils.waitForElement(driver, btnMove, 20);
		btnMove.click();
		ARCFNAAppUtils.waitForElement(driver, btnParentFolder, 20);
		btnParentFolder.click();
		ARCFNAAppUtils.waitForElement(driver, btnMove1, 20);
		if (btnMove1.isEnabled() == false) {
			ARCFNAAppUtils.waitForElement(driver, btnCancelMoveFile, 15);
			btnCancelMoveFile.click();
			return true;
		} else {
			ARCFNAAppUtils.waitForElement(driver, btnCancelMoveFile, 15);
			btnCancelMoveFile.click();
			return false;
		}

	}

	public boolean verifyFldrMoveToSameFldr() {
		ARCFNAAppUtils.waitForElement(driver, btnSelect, 20);
		btnSelect.click();
		ARCFNAAppUtils.waitForElement(driver, btnParentFolder, 20);
		btnParentFolder.click();
		ARCFNAAppUtils.waitForElement(driver, btnThreeDotted, 50);
		btnThreeDotted.click();
		ARCFNAAppUtils.waitForElement(driver, btnMove, 20);
		btnMove.click();
		ARCFNAAppUtils.waitForElement(driver, btnParentFolder, 20);
		if (btnMove1.isEnabled() == false) {
			ARCFNAAppUtils.waitForElement(driver, btnMove1, 20);
			btnCancelMoveFolder.click();
			return true;
		} else {
			ARCFNAAppUtils.waitForElement(driver, btnCancelMoveFolder, 20);
			btnCancelMoveFolder.click();
			return false;
		}

	}

	public boolean createFolder() {
		ARCFNAAppUtils.waitForElement(driver, btncreateFolder, 30);
		btncreateFolder.click();
		driver.switchTo().alert();
		driver.getKeyboard().sendKeys("Automation_Testing3");
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolder, 30);
		if (btncreatedFolder.isDisplayed()) {
			return true;
		} else {
			return false;
		}

	}

	public void createFolderForNameModification() {
		ARCFNAAppUtils.waitForElement(driver, btncreateFolder, 30);
		btncreateFolder.click();
		driver.switchTo().alert();
		driver.getKeyboard().sendKeys("Automation_Testing5");
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolder5, 30);
		btncreatedFolder5.isDisplayed();
	}

	public boolean verifyFolderNameChangeForRename() {
		TouchAction t1 = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolder5, 20);

		t1.longPress(btncreatedFolder5, 20000).release().perform();

		// ARCFNAAppUtils.waitTill(7000);
		// driver.getKeyboard().sendKeys(Keys.BACK_SPACE);
		driver.getKeyboard().sendKeys(Keys.DELETE);
		ARCFNAAppUtils.waitTill(1000);
		driver.getKeyboard().sendKeys("4");

		ARCFNAAppUtils.waitForElement(driver, btnFolderNameRename, 20);
		btnFolderNameRename.click();
		String actMsg = driver.switchTo().alert().getText();
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitTill(4000);
		String expMsg = "Folder already exists.";

		Log.message("*****************" + expMsg);
		Log.message("*****************" + actMsg);
		if (expMsg.equals(actMsg)) {
			driver.switchTo().alert().accept();
			return true;
		} else {
			driver.switchTo().alert().accept();
			return false;
		}
	}

	public boolean verifyDuplicateFolderCreation() {
		ARCFNAAppUtils.waitForElement(driver, btncreateFolder, 30);
		btncreateFolder.click();
		driver.switchTo().alert();
		driver.getKeyboard().sendKeys("Automation_Testing5");
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitTill(4000);
		String expMsg = "Folder already exists.";
		String actMsg = driver.switchTo().alert().getText();
		Log.message("*****************" + expMsg);
		Log.message("*****************" + actMsg);
		if (expMsg.equals(actMsg)) {
			driver.switchTo().alert().accept();
			return true;
		} else {
			driver.switchTo().alert().accept();
			return false;
		}
	}

	public boolean verifyDuplicateFolderRename() {
		ARCFNAAppUtils.waitForElement(driver, btncreateFolder, 30);
		btncreateFolder.click();
		driver.switchTo().alert();
		driver.getKeyboard().sendKeys("Automation_Testing5");
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitTill(4000);
		String expMsg = "Folder already exists.";
		String actMsg = driver.switchTo().alert().getText();
		Log.message("*****************" + expMsg);
		Log.message("*****************" + actMsg);
		if (expMsg.equals(actMsg)) {
			driver.switchTo().alert().accept();
			return true;
		} else {
			driver.switchTo().alert().accept();
			return false;
		}
	}

	public void deleteModifiedFolder() {
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolderNameChanged5, 20);
		int modFolderCnt = btncreatedFoldersNameChanged5.size();
		if (modFolderCnt > 0) {
			ARCFNAAppUtils.waitForElement(driver, btnThreeDotted, 50);
			btnThreeDotted.click();
			ARCFNAAppUtils.waitForElement(driver, btnSelect, 20);
			btnSelect.click();
			ARCFNAAppUtils.waitForElement(driver, btncreatedFolderNameChanged5, 30);
			btncreatedFolderNameChanged5.click();
			ARCFNAAppUtils.waitForElement(driver, btnThreeDotted, 50);
			btnThreeDotted.click();
			btnDelete.click();
			driver.switchTo().alert().accept();
			ARCFNAAppUtils.waitTill(3000);
		}
	}

	public void deleteDuplicateFolderTest() {
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolder5, 20);
		int modFolderCnt = btncreatedFolders5.size();
		if (modFolderCnt > 0) {
			ARCFNAAppUtils.waitForElement(driver, btnThreeDotted, 50);
			btnThreeDotted.click();
			ARCFNAAppUtils.waitForElement(driver, btnSelect, 20);
			btnSelect.click();
			ARCFNAAppUtils.waitForElement(driver, btncreatedFolder5, 30);
			btncreatedFolder5.click();
			ARCFNAAppUtils.waitForElement(driver, btnThreeDotted, 50);
			btnThreeDotted.click();
			btnDelete.click();
			driver.switchTo().alert().accept();
			ARCFNAAppUtils.waitTill(3000);
		}
	}

	public void folderNameChange() {
		TouchAction t1 = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolder5, 20);

		t1.longPress(btncreatedFolder5, 20000).release().perform();

		// ARCFNAAppUtils.waitTill(7000);
		// driver.getKeyboard().sendKeys(Keys.BACK_SPACE);
		driver.getKeyboard().sendKeys("_Changed");

		// ARCFNAAppUtils.waitForElement(driver, renameFolderTextBox, 30);
		// renameFolderTextBox.clear();
		// renameFolderTextBox.sendKeys("Automation_Testing5_Changed");

		ARCFNAAppUtils.waitForElement(driver, btnFolderNameRename, 20);
		btnFolderNameRename.click();
	}

	public boolean folderNameMatch() {
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolderNameChanged5, 20);
		String actFolderName = btncreatedFolderNameChanged5.getText();
		String expFolderName = "Automation_Testing5_Changed";

		if (actFolderName.equals(expFolderName)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean createFolder1() {
		ARCFNAAppUtils.waitForElement(driver, btncreateFolder, 30);
		btncreateFolder.click();
		driver.switchTo().alert();
		driver.getKeyboard().sendKeys("Automation_Testing4");
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolder1, 30);
		if (btncreatedFolder1.isDisplayed()) {
			return true;
		} else {
			return false;
		}

	}

	public boolean deleteFolder() {
		ARCFNAAppUtils.waitForListElement(driver, btncreatedFolders, 30);
		if (btncreatedFolders.size() > 0) {
			ARCFNAAppUtils.waitTill(3000);
			int noOfFoldersBeforeDelete = folderListPanel.findElements(By.name("FileListfileName")).size();
			Log.message(Integer.toString(noOfFoldersBeforeDelete));

			ARCFNAAppUtils.waitForElement(driver, btnThreeDotted, 50);

			btnThreeDotted.click();
			ARCFNAAppUtils.waitForElement(driver, btnSelect, 30);
			btnSelect.click();
			ARCFNAAppUtils.waitForElement(driver, btncreatedFolder, 30);
			btncreatedFolder.click();
			ARCFNAAppUtils.waitForElement(driver, btnThreeDotted, 50);
			btnThreeDotted.click();
			btnDelete.click();
			driver.switchTo().alert().accept();
			ARCFNAAppUtils.waitTill(3000);
			int noOfFoldersAfterDelete = folderListPanel.findElements(By.name("FileListfileName")).size();
			Log.message(Integer.toString(noOfFoldersAfterDelete));

			// ARCFNAAppUtils.waitForListElement(driver, btncreatedFolders, 10);
			if (noOfFoldersBeforeDelete > noOfFoldersAfterDelete) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	public boolean deleteFolder1() {
		ARCFNAAppUtils.waitForListElement(driver, btncreatedFolders1, 30);
		if (btncreatedFolders1.size() > 0) {
			ARCFNAAppUtils.waitTill(3000);
			int noOfFoldersBeforeDelete = folderListPanel.findElements(By.name("FileListfileName")).size();
			ARCFNAAppUtils.waitForElement(driver, btnThreeDotted, 50);
			btnThreeDotted.click();
			ARCFNAAppUtils.waitForElement(driver, btnSelect, 30);
			btnSelect.click();
			ARCFNAAppUtils.waitForElement(driver, btncreatedFolder1, 30);
			btncreatedFolder1.click();
			ARCFNAAppUtils.waitForElement(driver, btnThreeDotted, 50);
			TouchAction action7 = new TouchAction(driver);
			action7.press(btnThreeDotted).release().perform();
			action7.release();
			ARCFNAAppUtils.waitForElement(driver, btnDelete, 30);
			btnDelete.click();
			driver.switchTo().alert().accept();
			int noOfFoldersAfterDelete = folderListPanel.findElements(By.name("FileListfileName")).size();
			Log.message(Integer.toString(noOfFoldersAfterDelete));

			// ARCFNAAppUtils.waitForListElement(driver, btncreatedFolders, 10);
			if (noOfFoldersBeforeDelete > noOfFoldersAfterDelete) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	public void clickOnCreatedFolder() {
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolder, 30);
		btncreatedFolder.click();
	}

	public boolean uploadFile() {
		ARCFNAAppUtils.waitForElement(driver, btnUploadFile, 20);
		btnUploadFile.click();
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitTill(3000);
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitTill(3000);
		ARCFNAAppUtils.waitForElement(driver, btnPhotoCapture, 20);
		btnPhotoCapture.click();
		ARCFNAAppUtils.waitForElement(driver, btnUsePhoto, 20);
		btnUsePhoto.click();
		ARCFNAAppUtils.waitForElement(driver, btnSkip, 20);
		btnSkip.click();
		ARCFNAAppUtils.waitTill(40000);
		ARCFNAAppUtils.waitForElement(driver, uploadedPhoto, 200);
		if (uploadedPhoto.isEnabled()) {
			return true;
		} else {
			return false;
		}

	}

	public void returnFolderPage() throws InterruptedException {

		// rootFolderNavigation.click();
		ARCFNAAppUtils.waitForElement(driver, rootFolderNavigation, 120);
		/*
		 * TouchAction action8 = new TouchAction(driver);
		 * action8.press(rootFolderNavigation).release().perform(); action8.release();
		 */
		rootFolderNavigation.click();

	}

	public boolean shareFolder() {
		ARCFNAAppUtils.waitForElement(driver, btnSelect, 20);
		btnSelect.click();
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolder, 20);
		btncreatedFolder.click();
		btnThreeDotted.click();
		ARCFNAAppUtils.waitForElement(driver, btnShare, 20);
		btnShare.click();
		ARCFNAAppUtils.waitForElement(driver, btnCancelIOSMailUI, 30);
		// ARCFNAAppUtils.waitTill(5000);
		if (btnCancelIOSMailUI.isDisplayed()) {
			Log.message("Cancel button is displayed");
			/*
			 * int leftX = btnCancelIOSMailUI.getLocation().getX(); int rightX = leftX +
			 * btnCancelIOSMailUI.getSize().getWidth(); int middleX = (rightX + leftX) / 2;
			 * int upperY = btnCancelIOSMailUI.getLocation().getY(); int lowerY = upperY +
			 * btnCancelIOSMailUI.getSize().getHeight(); int middleY = (upperY + lowerY) /
			 * 2; TouchAction t1 = new TouchAction(driver);
			 * Log.message(Integer.toString(middleX));
			 * Log.message(Integer.toString(middleY));
			 */

			TouchAction t1 = new TouchAction(driver);
			t1.press(170, 38).perform();
			ARCFNAAppUtils.waitForElement(driver, btnDeleteMailDraft, 40);
			btnDeleteMailDraft.click();
			return true;
		} else {
			return false;
		}
	}

	public boolean moveFolder() {
		ARCFNAAppUtils.waitForElement(driver, btnSelect, 20);
		btnSelect.click();
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolder, 20);
		btncreatedFolder.click();
		btnThreeDotted.click();
		ARCFNAAppUtils.waitForElement(driver, btnMove, 20);
		btnMove.click();
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolder1, 20);
		btncreatedFolder1.click();
		ARCFNAAppUtils.waitForElement(driver, btnMove1, 30);
		btnMove1.click();
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolder1, 20);
		btncreatedFolder1.click();
		ARCFNAAppUtils.waitForElement(driver, btncreatedFolder, 30);
		if (btncreatedFolder.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}

	public void hyperlinkFolderSelection() {
		ARCFNAAppUtils.waitForElement(driver, btnHyperlinkFileSearch, 20);
		btnHyperlinkFileSearch.click();
		driver.getKeyboard().sendKeys("Automation_Testing2");
		ARCFNAAppUtils.waitForElement(driver, hyperlinkFolderName, 20);
		hyperlinkFolderName.click();
		ARCFNAAppUtils.waitForElement(driver, btnLink1, 20);
		btnLink1.click();
		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		btnAnnotationSave.click();
		driver.switchTo().alert();

		txtBoxMarkupSaving.sendKeys("HyperlinkAnnotation6");

		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
		Log.message("Desired folder selection is done");

	}

	public boolean verifyHyperlinkFolder() {
		ARCFNAAppUtils.waitTill(2000);
		TouchAction action1 = new TouchAction(driver);
		action1.press(481, 407).release().perform();
		Log.message("Hyperlink clicked");
		ARCFNAAppUtils.waitForElement(driver, fileNameThumbnail, 60);
		fileNameThumbnail.click();
		ARCFNAAppUtils.waitForElement(driver, hyperlinkFolderName1, 40);
		if (hyperlinkFolderName1.isDisplayed()) {
			Log.message("The hyperlinked folder opened successfully");
			return true;

		} else {
			Log.message("The hyperlinked folder not opened successfully");
			return false;
		}
	}

	public void hideViewAllMarkup() {
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();
		ARCFNAAppUtils.waitForElement(driver, btnViewAllWithOutCash, 50);
		btnViewAllWithOutCash.click();
		ARCFNAAppUtils.waitTill(2000);
	}

	public boolean verifyExternalHyperlink() {
		TouchAction action1 = new TouchAction(driver);
		action1.press(481, 407).release().perform();
		action1.release();
		ARCFNAAppUtils.waitForElement(driver, btnExternalhyperlink, 20);

		int leftX = btnExternalhyperlink.getLocation().getX();
		int rightX = leftX + btnExternalhyperlink.getSize().getWidth();
		int middleX = (rightX + leftX) / 2;
		// Log.message("&&&&&&&&&&&&&&&&&&&&&&&&&& middleX");

		int upperY = btnExternalhyperlink.getLocation().getY();
		int lowerY = upperY + btnExternalhyperlink.getSize().getHeight();
		int middleY = (upperY + lowerY) / 2;
		// Log.message("&&&&&&&&&&&&&&&&&&&&&&&&& &middleY");

		TouchAction action2 = new TouchAction(driver);
		action2.press(middleX, middleY).release().perform();
		action2.release();

		// btnExternalhyperlink.click();
		Log.message("Hyperlink clicked");
		ARCFNAAppUtils.waitForElement(driver, capGoogle, 30);
		if (capGoogle.isEnabled()) {
			ARCFNAAppUtils.waitForElement(driver, btnReturnFacilityArchiveGooglePage, 20);
			btnReturnFacilityArchiveGooglePage.click();
			return true;
		} else {
			ARCFNAAppUtils.waitForElement(driver, btnReturnFacilityArchiveGooglePage, 20);
			btnReturnFacilityArchiveGooglePage.click();
			return false;
		}
	}

	@SuppressWarnings("unused")
	public boolean checkingMultiSavedCalibratorMarkup() throws InterruptedException, IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingCalibrator.png"));
		// Thread.sleep(2000);
		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingCalibrator.png");
		Log.message("Screenshot taken of saved markup");
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();

		ARCFNAAppUtils.waitForElement(driver, btnHideAll, 20);
		btnHideAll.click();
		btnViewDropDown.click();
		ARCFNAAppUtils.waitForElement(driver, btnViewAll, 20);
		btnViewAll.click();

		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingCalibrator.png"));
//	Thread.sleep(2000);
		ARCFNAAppUtils.cropImage(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingCalibrator.png");
		Log.message("Screenshot taken of opened markup");
		// btnViewDropDown.click();
		// selectSavedMarkup.click();
		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingCalibrator.png"));
		// Thread.sleep(2000);
		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingCalibrator.png"));
//	Thread.sleep(2000);
		Log.message("Checking the height and width of both the images are same?");
		Log.message("Width of image before saving:" + " " + Integer.toString(imgA.getWidth()));
		Log.message("Width of image after saving:" + " " + Integer.toString(imgB.getWidth()));
		Log.message("Height of image before saving:" + " " + Integer.toString(imgA.getHeight()));
		Log.message("Height of image after saving:" + " " + Integer.toString(imgB.getHeight()));

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public void calibratorMesureLineDrawing() {
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureLine, 20);
		btnCalibratorMeasureLine.click();

		TouchAction action1 = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 30);
		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			// action.press(276, 577).moveTo(100,100).release().perform();
			action1.longPress(490, 407).moveTo(100, 100).release().perform();
			Log.message("Annotation drawn using Square infolink");
		}
		action1.release();

	}

	public void calibratorMesureRectDrawing() {
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureRect, 20);
		btnCalibratorMeasureRect.click();

		TouchAction action2 = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 30);
		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			// action.press(276, 577).moveTo(100,100).release().perform();
			action2.longPress(500, 407).moveTo(100, 100).release().perform();
			Log.message("Annotation drawn using Square infolink");
		}
		action2.release();

	}

	public void calibratorMesureCircleDrawing() {
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureCircle, 20);
		btnCalibratorMeasureCircle.click();

		TouchAction action3 = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 30);
		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			// action.press(276, 577).moveTo(100,100).release().perform();
			action3.longPress(520, 407).moveTo(100, 100).release().perform();
			Log.message("Annotation drawn using Square infolink");
		}
		action3.release();

	}

	public void calibratorMesureFreehandDrawing() {
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureFreehand, 20);
		btnCalibratorMeasureFreehand.click();

		TouchAction action4 = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 30);
		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			// action.press(276, 577).moveTo(100,100).release().perform();
			action4.longPress(515, 407).moveTo(100, 100).release().perform();
			Log.message("Annotation drawn using Square infolink");
		}
		action4.release();

	}

	public void calibratorMesureAreaDrawing() {
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureArea, 20);
		btnCalibratorMeasureArea.click();

		TouchAction action5 = new TouchAction(driver);
		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 30);
		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			// action.press(276, 577).moveTo(100,100).release().perform();
			action5.longPress(530, 407).moveTo(100, 100).release().perform();
			Log.message("Annotation drawn using Square infolink");
		}
		action5.release();

	}

	public void calibratorWithRefSaving() {
		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		driver.switchTo().alert();
		txtBoxMarkupSaving.sendKeys("CalibratorAnnotationwithref");
		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
		ARCFNAAppUtils.waitTill(3000);
	}

	@SuppressWarnings("unused")
	public boolean checkingSavedCalibratorMarkupWithRef() throws InterruptedException, IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingCalibrator.png"));
		// Thread.sleep(2000);
		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingCalibrator.png");
		Log.message("Screenshot taken of saved markup");
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();

		ARCFNAAppUtils.waitForElement(driver, btnHideAll, 20);
		btnHideAll.click();
		btnViewDropDown.click();
		ARCFNAAppUtils.waitForElement(driver, btnViewAll, 20);
		btnViewAll.click();

		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingCalibrator.png"));
//	Thread.sleep(2000);
		ARCFNAAppUtils.cropImage(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingCalibrator.png");
		Log.message("Screenshot taken of opened markup");
		// btnViewDropDown.click();
		// selectSavedMarkup.click();
		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingCalibrator.png"));
		// Thread.sleep(2000);
		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingCalibrator.png"));
//	Thread.sleep(2000);
		Log.message("Checking the height and width of both the images are same?");
		Log.message("Width of image before saving:" + " " + Integer.toString(imgA.getWidth()));
		Log.message("Width of image after saving:" + " " + Integer.toString(imgB.getWidth()));
		Log.message("Height of image before saving:" + " " + Integer.toString(imgA.getHeight()));
		Log.message("Height of image after saving:" + " " + Integer.toString(imgB.getHeight()));

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public void tapOnCalibrator() {
		// Screen screen = new Screen();
		ARCFNAAppUtils.waitTill(3000);
		TouchAction action1 = new TouchAction(driver);

		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			action1.press(481, 407).release().perform();
			// action1.tap(481, 407).release().perform();
		}
		action1.release();
	}

	public boolean tapOnCalibrator1() {
		// Screen screen = new Screen();
		TouchAction action1 = new TouchAction(driver);

		if (inputCanvas.isDisplayed()) {
			Log.message("**********************Input Canvas has been displayed*********************");
			// action.press(276, 577).moveTo(100,100).release().perform();
			action1.tap(500, 407).release().perform();
		}
		action1.release();

		if (btnSetActiveCalibrators.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean verifyCalibratorTapOption() {
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorRecalibrate, 20);
		if (btnCalibratorRecalibrate.isDisplayed() && btnCalibratorClearall.isDisplayed()
				&& btnCalibratorDelete.isDisplayed() && btnCalibratorSetting.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("unlikely-arg-type")
	public boolean verifyRecalibrate() {
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorRecalibrate, 20);
		btnCalibratorRecalibrate.click();
		ARCFNAAppUtils.waitForElement(driver, txtFeetWithOutCache, 20);
		// txtFeet.clear();
		driver.getKeyboard().pressKey(Keys.BACK_SPACE);
		txtFeetWithOutCache.sendKeys("8");
		ARCFNAAppUtils.waitForElement(driver, textInchesWithOutCache, 40);
		ARCFNAAppUtils.waitTill(3000);
		textInchesWithOutCache.clear();

		// driver.getKeyboard().pressKey(Keys.BACK_SPACE);
		textInchesWithOutCache.sendKeys("3");
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorDone, 20);
		btnCalibratorDone.click();
		ARCFNAAppUtils.waitTill(3000);
		TouchAction action2 = new TouchAction(driver);
		action2.press(481, 407).release().perform();
		ARCFNAAppUtils.waitTill(3000);
		TouchAction action3 = new TouchAction(driver);
		action3.press(481, 407).release().perform();
		// action2.tap(276, 577).release().perform();
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorRecalibrate, 20);
		btnCalibratorRecalibrate.click();

		action2.release();
		/*
		 * TouchAction action3 = new TouchAction(driver); if(inputCanvas.isDisplayed())
		 * { action3.press(481, 407).release().perform(); //action2.tap(276,
		 * 577).release().perform(); btnCalibratorRecalibrate.click(); }
		 * action3.release();
		 */

		ARCFNAAppUtils.waitForElement(driver, txtFeetWithOutCache, 20);

		int feetText = Integer.parseInt(txtFeetWithOutCache.getText());

		Log.message(txtFeetWithOutCache.getText());

		ARCFNAAppUtils.waitForElement(driver, textInchesWithOutCache, 20);

		int inchText = Integer.parseInt(textInchesWithOutCache.getText());

		Log.message(textInchesWithOutCache.getText());

		if (feetText == 8 && inchText == 3) {
			ARCFNAAppUtils.waitForElement(driver, btnCalibratorDone, 10);
			btnCalibratorDone.click();
			return true;
		} else {
			ARCFNAAppUtils.waitForElement(driver, btnCalibratorDone, 10);
			btnCalibratorDone.click();
			return false;
		}

	}

	public void refLineCalibratorSaving1() {
		ARCFNAAppUtils.waitForElement(driver, btnAnnotationSave, 20);
		btnAnnotationSave.click();
		driver.switchTo().alert();
		txtBoxMarkupSaving.sendKeys("RefLineCalibratorAnnotation1");
		driver.switchTo().alert().accept();
		Log.message("Annotation drawn and saved");
	}

	public void refLineCalibratorSaving1Meter() throws IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/RefLineCalibratorMeter.png"));
		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./TemplateScreenShots/RefLineCalibratorMeter.png");
	}

	public void refLineCalibratorSaving1Feet() throws IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/RefLineCalibratorFeet.png"));
		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./TemplateScreenShots/RefLineCalibratorFeetTemplate.png");
	}

	public void setActiveCalibrator() {
		ARCFNAAppUtils.waitForElement(driver, btnSetActiveCalibrator, 20);
		btnSetActiveCalibrator.click();
	}

	@SuppressWarnings("unused")
	public boolean verifyRefLineCalibratorMetric() throws IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/SavingMetricCalibrator.png"));
		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/SavingMetricCalibrator.png");

		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/SavingMetricCalibrator.png"));
		BufferedImage imgB = ImageIO.read(new File("./TemplateScreenShots/RefLineCalibratorMeter.png"));

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	@SuppressWarnings("unused")
	public boolean verifyRefLineCalibratorImperial() throws IOException {
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/SavingImperialCalibrator.png"));
		ARCFNAAppUtils.cropImage(srcFiler, docViewer, "./FinalCroppedScreenShots/SavingImperialCalibrator.png");

		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/SavingImperialCalibrator.png"));
		BufferedImage imgB = ImageIO.read(new File("./TemplateScreenShots/RefLineCalibratorFeetTemplate.png"));

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						// ARCFNAAppUtils.waitTill(4000);
						// ARCFNAAppUtils.deleteInitialScreenShotFiles();
						// ARCFNAAppUtils.waitTill(2000);
						// ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						// ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;

	}

	public boolean initialCalibratorRulerVisibility() {
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorSubOption, 20);
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureArea, 20);
		// ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureFreehand, 20);
		// ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureCircle, 20);
		// ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureRect, 20);
		// ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureLine, 20);

		if (btnCalibratorSubOption.isEnabled() && btnCalibratorMeasureArea.isEnabled() == false
				&& btnCalibratorMeasureFreehand.isEnabled() == false && btnCalibratorMeasureCircle.isEnabled() == false
				&& btnCalibratorMeasureRect.isEnabled() == false && btnCalibratorMeasureLine.isEnabled() == false) {
			return true;
		} else {
			return false;
		}
	}

	public boolean verifyStateRefCalibratorWithoutRuler() {
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorSubOption, 20);
		ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureArea, 20);
		// ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureFreehand, 20);
		// ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureCircle, 20);
		// ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureRect, 20);
		// ARCFNAAppUtils.waitForElement(driver, btnCalibratorMeasureLine, 20);

		if (btnCalibratorMeasureArea.isEnabled() == false && btnCalibratorMeasureFreehand.isEnabled() == false
				&& btnCalibratorMeasureCircle.isEnabled() == false && btnCalibratorMeasureRect.isEnabled() == false
				&& btnCalibratorMeasureLine.isEnabled() == false) {
			return true;
		} else {
			return false;
		}
	}

	@SuppressWarnings("unused")
	public boolean verifyRulerNRefCalibratorInDiffMarkup() throws IOException {

		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingRulerNRefCalibratorInDiffFile.png"));
		ARCFNAAppUtils.cropImage(srcFiler, docViewer,
				"./FinalCroppedScreenShots/BeforeSavingRulerNRefCalibratorInDiffFile.png");

		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();
		btnViewAllWithOutCash.click();
		btnViewDropDown.click();
		btnViewAllWithOutCash.click();
		ARCFNAAppUtils.waitTill(3000);

		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingRulerNRefCalibratorInDiffFile.png"));
		ARCFNAAppUtils.cropImage(srcFiler1, docViewer,
				"./FinalCroppedScreenShots/AfterSavingRulerNRefCalibratorInDiffFile.png");

		BufferedImage imgA = ImageIO
				.read(new File("./FinalCroppedScreenShots/BeforeSavingRulerNRefCalibratorInDiffFile.png"));
		BufferedImage imgB = ImageIO
				.read(new File("./FinalCroppedScreenShots/AfterSavingRulerNRefCalibratorInDiffFile.png"));

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		ARCFNAAppUtils.waitTill(3000);
		return true;

	}

	public boolean verifyDocumentExportWithCalibrator() {
		ARCFNAAppUtils.waitForElement(driver, btnExportSend, 20);
		btnExportSend.click();
		ARCFNAAppUtils.waitForElement(driver, btnPDFDocument, 20);
		TouchAction action3 = new TouchAction(driver);
		action3.tap(btnPDFDocument).release().perform();
		action3.release();
		// btnPDFDocument.click();
		ARCFNAAppUtils.waitForElement(driver, btnPDFDocumentType, 20);
		btnPDFDocumentType.click();
		ARCFNAAppUtils.waitForElement(driver, txtFieldTo, 20);
		txtFieldTo.click();
		driver.getKeyboard().sendKeys("subhendu.das@e-arc.com");
		ARCFNAAppUtils.waitForElement(driver, btnMailSend, 20);
		btnMailSend.click();
		ARCFNAAppUtils.waitTill(5000);
		String msgExport = driver.switchTo().alert().getText();
		if (msgExport.equalsIgnoreCase("Your mail has been sent successfully.")) {
			driver.switchTo().alert().accept();
			return true;
		} else {
			driver.switchTo().alert().accept();
			return false;
		}
	}

	public boolean verifyRotationSaveDisabled() {
		ARCFNAAppUtils.waitForElement(driver, btnRotationSave, 20);
		if (btnRotationSave.isEnabled() == false) {
			return true;
		} else {
			return false;
		}
	}

	public void rotateLeft() {
		ARCFNAAppUtils.waitForElement(driver, btnRotateLeft, 20);
		TouchAction action5 = new TouchAction(driver);
		action5.tap(btnRotateLeft).release().perform();
		action5.release();
		// btnRotateLeft.click();
	}

	public void calcelRotation() {
		ARCFNAAppUtils.waitForElement(driver, btnRotationCancel, 20);
		btnRotationCancel.click();
	}

	public boolean verifyRotationoptionAvailable() {
		ARCFNAAppUtils.waitForElement(driver, rotationToolbar, 50);
		if (rotationToolbar.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean verifyRotationSaving() throws IOException {

		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 30);
		int widthBeforeRotation = inputCanvas.getSize().getWidth();
		Log.message("Width before saving rotation" + " " + widthBeforeRotation);
		int heightBeforeRotation = inputCanvas.getSize().getHeight();
		Log.message("Height before saving rotation" + " " + heightBeforeRotation);

		ARCFNAAppUtils.waitForElement(driver, rotationToolbar, 50);
		rotationToolbar.click();

		ARCFNAAppUtils.waitTill(3000);

		ARCFNAAppUtils.waitForElement(driver, btnRotateLeft, 20);
		btnRotateLeft.click();

		ARCFNAAppUtils.waitForElement(driver, rotationSaveButton, 20);
		rotationSaveButton.click();

		ARCFNAAppUtils.waitForElement(driver, inputCanvas, 20);
		int widthAfterRotation = inputCanvas.getSize().getWidth();
		Log.message("Width after saving rotation" + " " + widthAfterRotation);
		int heightAfterRotation = inputCanvas.getSize().getHeight();
		Log.message("Height after saving rotation" + " " + heightBeforeRotation);

		if (widthBeforeRotation != widthAfterRotation || heightBeforeRotation != heightAfterRotation) {
			ARCFNAAppUtils.waitTill(3000);
			ARCFNAAppUtils.waitForElement(driver, rotationToolbar, 50);
			rotationToolbar.click();
			ARCFNAAppUtils.waitForElement(driver, rotationRightButton, 20);
			rotationRightButton.click();
			ARCFNAAppUtils.waitForElement(driver, rotationSaveButton, 20);
			rotationSaveButton.click();
			return true;
		} else {
			ARCFNAAppUtils.waitTill(3000);
			ARCFNAAppUtils.waitForElement(driver, rotationToolbar, 50);
			rotationToolbar.click();
			ARCFNAAppUtils.waitForElement(driver, rotationRightButton, 20);
			rotationRightButton.click();
			ARCFNAAppUtils.waitForElement(driver, rotationSaveButton, 20);
			rotationSaveButton.click();
			return false;
		}

	}

	@SuppressWarnings("unused")
	public boolean verifyFileInfo() {
		ARCFNAAppUtils.waitForElement(driver, btnFileListInfo, 20);
		int noBtnInfo = btnFileListInfos.size();
		for (int i = 0; i <= noBtnInfo; i++) {
			btnFileListInfo.click();
			break;
		}

		ARCFNAAppUtils.waitForElement(driver, btnRevisionhistory, 30);
		if (btnRevisionhistory.isDisplayed()) {
			return true;
		} else {
			return false;
		}

	}

	public void closeFileInfo() {
		ARCFNAAppUtils.waitForElement(driver, fileInfoClose, 30);
		fileInfoClose.click();
	}

	public void verifyExternalHyperlinkFunctionality() {
		TouchAction action1 = new TouchAction(driver);
		action1.press(319, 622).release().perform();
		Log.message("External Hyperlink clicked");

	}

	@SuppressWarnings("unused")
	public void buttonSyncOfflineRemove() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnSyncOffline, 20);
		// btnSyncOffline.click();
		int noOfBtnSync = btnSyncOfflines.size();
		for (int i = 0; i <= noOfBtnSync; i++) {
			btnSyncOffline.click();
			// Thread.sleep(10000);
			ARCFNAAppUtils.waitTill(10000);
			break;
		}
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitTill(4000);
	}

	@SuppressWarnings("unused")
	public void buttonSyncOffline() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnSyncOffline, 20);
		// btnSyncOffline.click();
		int noOfBtnSync = btnSyncOfflines.size();
		for (int i = 0; i <= noOfBtnSync; i++) {
			btnSyncOffline.click();
			// Thread.sleep(10000);
			ARCFNAAppUtils.waitTill(10000);
			break;
		}
	}

	@SuppressWarnings("unused")
	public void buttonSyncOnline() throws InterruptedException {
		ARCFNAAppUtils.waitForElement(driver, btnSyncOffline, 20);
		// btnSyncOffline.click();
		int noOfBtnSync = btnSyncOfflines.size();
		for (int i = 0; i <= noOfBtnSync; i++) {
			btnSyncOffline.click();
			// Thread.sleep(10000);
			ARCFNAAppUtils.waitTill(2000);
			driver.switchTo().alert().accept();
			ARCFNAAppUtils.waitTill(4000);
			break;
		}
	}

	public void removeClickedAlertMessage() {
		driver.switchTo().alert().accept();
	}

	public boolean verifyFileSyncRemoval() {
		ARCFNAAppUtils.waitForElement(driver, captionSyncOffline, 20);
		if (captionSyncOffline.getText().equals("Sync Offline"))
			return true;
		else
			return false;
	}

	public boolean verifyCollectionOpenOfflineMode() {
		ARCFNAAppUtils.waitForElement(driver, btnFile, 20);
		if (btnFile.isDisplayed())
			return true;
		else
			return false;

	}

	public boolean verifyCollectionOpenOfflineMode1() {
		ARCFNAAppUtils.waitForElement(driver, btnFile, 20);
		if (btnFile.isDisplayed())
			return true;
		else
			return false;

	}

	public boolean openSyncedFileOffLineMode() {
		ARCFNAAppUtils.waitForElement(driver, hyperLinkfileName3, 30);
		if (hyperLinkfileName3.isDisplayed())
			return true;
		else
			return false;
	}

	public void closeViewer() {
		ARCFNAAppUtils.waitForElement(driver, btnCloseViewer, 20);
		btnCloseViewer.click();
	}

	public void closeViewerFolderLevel() {
		ARCFNAAppUtils.waitForElement(driver, closeViewerFolderLevel, 20);
		closeViewerFolderLevel.click();
	}

	public void closeViewerWithUnsavedMarkups() {
		ARCFNAAppUtils.waitForElement(driver, btnCloseViewer, 20);
		btnCloseViewer.click();
		ARCFNAAppUtils.waitTill(2000);
		driver.switchTo().alert().dismiss();
	}

	public void clickSelect() {
		ARCFNAAppUtils.waitForElement(driver, btnSelect, 20);
		btnSelect.click();
	}

	public void selectSyncedFile() {
		ARCFNAAppUtils.waitForElement(driver, btnFile, 20);
		btnFile.click();
	}

	public void selectUnSyncedFile() {
		ARCFNAAppUtils.waitForElement(driver, fileToBeLinked, 20);
		fileToBeLinked.click();
	}

	public void selectFolder() {
		ARCFNAAppUtils.waitForElement(driver, btnParentFolder, 20);
		btnParentFolder.click();
	}

	public void deSelect() {
		ARCFNAAppUtils.waitForElement(driver, btnDeselect, 20);
		btnDeselect.click();
	}

	public void clickMove() {
		ARCFNAAppUtils.waitForElement(driver, btnMove, 20);
		btnMove.click();
	}

	public boolean verifyMoveSyncedFileOffline() {
		String actualMessage = "Unable to connect to SKYSITE. Please check your internet connection.";
		String expectedMessage = driver.switchTo().alert().getText();
		if (actualMessage.equals(expectedMessage)) {
			driver.switchTo().alert().accept();
			return true;
		} else {
			driver.switchTo().alert().accept();
			return false;
		}
	}

	public boolean verifyCalibratorRulerDrawingSavingImperial() throws IOException {
		ARCFNAAppUtils.waitTill(3000);
		Log.message("****1****");
		File srcFiler = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		Log.message("****2****");
		FileUtils.copyFile(srcFiler, new File("./InitialScreenShots/BeforeSavingMarkup.png"));
		Log.message("****3****");
		ARCFNAAppUtils.cropImageZoom(srcFiler, docViewer, "./FinalCroppedScreenShots/BeforeSavingMarkup.png");
		Log.message("****4****");
		Log.message("Screenshot taken of saved markup");
		ARCFNAAppUtils.waitForElement(driver, btnViewDropDown, 50);
		btnViewDropDown.click();
		TouchAction a1 = new TouchAction(driver);
		a1.press(100, 206).release().perform();
		btnViewDropDown.click();
		a1.press(100, 206).release().perform();
		Log.message("Saved markup has been opned using viewer drop down");
		ARCFNAAppUtils.waitTill(2000);
		File srcFiler1 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(srcFiler1, new File("./InitialScreenShots/AfterSavingMarkup.png"));
		ARCFNAAppUtils.cropImageZoom(srcFiler1, docViewer, "./FinalCroppedScreenShots/AfterSavingMarkup.png");
		Log.message("Screenshot taken of opened markup");
		BufferedImage imgA = ImageIO.read(new File("./FinalCroppedScreenShots/BeforeSavingMarkup.png"));
		BufferedImage imgB = ImageIO.read(new File("./FinalCroppedScreenShots/AfterSavingMarkup.png"));
		Log.message("Checking the height and width of both the images are same?");
		Log.message("Width of image before saving:" + " " + Integer.toString(imgA.getWidth()));
		Log.message("Width of image after saving:" + " " + Integer.toString(imgB.getWidth()));
		Log.message("Height of image before saving:" + " " + Integer.toString(imgA.getHeight()));
		Log.message("Height of image after saving:" + " " + Integer.toString(imgB.getHeight()));

		if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
			Log.message("Image height and width are same");
			for (int x = 0; x < imgA.getWidth(); x++) {
				for (int y = 0; y < imgB.getHeight(); y++) {
					if (imgA.getRGB(x, y) != imgB.getRGB(x, y)) {
						int count = 0;
						Log.message("Mismatched pixel count" + " " + count + 1);
						Log.message("Both the images are different");
						return false;
					} else {
						Log.message("Both the images are same");
						ARCFNAAppUtils.waitTill(4000);
						ARCFNAAppUtils.deleteInitialScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						ARCFNAAppUtils.deleteCroppedScreenShotFiles();
						ARCFNAAppUtils.waitTill(2000);
						return true;
					}
				}
			}
		}
		// action.tap(inputCanvas).

		return true;
	}

	private WebElement chooseIcon(String iconName) {
		return driver.findElement(By.id(iconName));
	}

	private WebElement selectCollection(String collectionName) {
		return driver.findElement(By.id(collectionName));
	}

	private WebElement selectfolder(String folderName) {
		return driver.findElement(By.id(folderName));
	}

	public void openTagCreationPopup() {
		// Clicking on the photo tag annotation icon
		ARCFNAAppUtils.waitForElement(driver, tagIcon, 20);
		tagIcon.click();
		Log.message("Clicked on photo tagging icon !!");
		// Clicking on the image to create photo tag
		TouchAction action = new TouchAction(driver);
		action.press(481, 407).release().perform();
		Log.message("Photo tagging annotation clicked !!");
		action.release();
	}

	public void changeIcon(String iconName) {
		// Clicking on the change icon
		tag_ChangeIcon.click();
		ARCFNAAppUtils.waitForElement(driver, tag_SearchTxtBox, 20);
		chooseIcon(iconName).click();
		Log.message(iconName + "icon from list clicked");
		doneBtn.click();
		Log.message("Done button clicked !!");
	}

	public boolean verifyIcon(String iconName) {
		ARCFNAAppUtils.waitForElement(driver, tag_SaveBtn, 20);
		if (chooseIcon(iconName).isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	public void closeTagPopup() {
		ARCFNAAppUtils.waitForElement(driver, tag_CancelBtn, 20);
		tag_CancelBtn.click();
	}
	
	
	public EquipmentPage launchEquipment()
	{
		Log.message("Launching equipment management");
		ARCFNAAppUtils.waitForElement(driver, btnEquipment, 20);
		btnEquipment.click();
		return new EquipmentPage(driver).get();
	}
	
	public void menuButtonClick()
	{
		ARCFNAAppUtils.waitForElement(driver, btnMenu, 30);
		Log.message("Clicking on Menu button");
		btnMenu.click();
	}
		

	public void searchIcon(String iconName) {
		tag_ChangeIcon.click();
		ARCFNAAppUtils.waitForElement(driver, tag_SearchTxtBox, 20);
		tag_SearchTxtBox.sendKeys(iconName);
		chooseIcon(iconName).click();
		Log.message(iconName + "icon from list clicked");
		doneBtn.click();
		Log.message("Done button clicked !!");
	}


}

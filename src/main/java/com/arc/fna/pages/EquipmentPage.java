package com.arc.fna.pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.arc.fna.utils.ARCFNAAppUtils;
import com.arcautoframe.utils.Log;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class EquipmentPage extends LoadableComponent<EquipmentPage>{
	
	public static AppiumDriver<MobileElement> driver;
	private boolean isPageLoaded;
	
	@iOSFindBy(accessibility="Equipment")
	WebElement equimentCaption;
	
	@iOSFindBy(accessibility="EquipmentNavigationBarLeftBtn")
	WebElement btnMenu;
	
	@iOSFindBy(accessibility="SignOutMenu")
	WebElement btnSignOut;
	
	//-----------------------------
	
	@iOSFindBy(accessibility="EquipmentNavigationBarRightBtn")
	WebElement btnAddEquipment;
	
	@iOSFindBy(accessibility="AssetDetailsIconPhotoVideoCellFullBtn")
	WebElement btnAddPhotoEquipment;
	
	@iOSFindBy(accessibility="CustomCameraVCCameraMainBtn")
	WebElement btnCameraEquipment;
	
	@iOSFindBy(accessibility="CustomCameraVCImageBtn")
	WebElement btnMarkupDraw;
	
	@iOSFindBy(accessibility="PWPImageHeaderViewDoneBtn")
	WebElement btnMarkupDone;
	
	@iOSFindBy(accessibility="PWPImageHeaderViewDeleteBtn")
	WebElement btnMarkupDelete;
	
	@iOSFindBy(accessibility="PWPImageHeaderViewCloseBtn")
	WebElement btnMarkupClose;
	
	@iOSFindBy(accessibility="AssetDetailsInstructionCellTitleLbl")
	List<WebElement> lblEquipmentInfoList;
	
	@iOSFindBy(accessibility="AssetDetailsInstructionCellTitleLbl")
	WebElement lblEquipmentInfo;
	
	@iOSFindBy(accessibility="AssetDetailsInstructionCellInstructionBtn")
	List<WebElement> btnsEquipmentAllDetailsAdd;
	
	@iOSFindBy(accessibility="AssetDetailsInstructionCellInstructionBtn")
	WebElement btnEquipmentAllDetailsAdd;
	
	@iOSFindBy(accessibility="ImageViewerHeaderCellSelectBtn_1006")
	WebElement btnLineDraw;
	
	
	
	
	
	
	
	

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		
	}
	
	public EquipmentPage(AppiumDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
		//PageFactory.initElements(this.driver, this);
	}
	
	
	public boolean verifyEquipmentModulelaunch()
	{
		Log.message("Checking whether equipment caption is visible?");
		ARCFNAAppUtils.waitForElement(driver, equimentCaption, 30);
		if(equimentCaption.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void equipmentAdd()
	{
		ARCFNAAppUtils.waitForElement(driver, btnAddEquipment, 40);
		btnAddEquipment.click();	
	}	
	
	public void equipmentPhotoAddWithLineMarkup()
	{
		ARCFNAAppUtils.waitForElement(driver, btnAddPhotoEquipment, 40);
		btnAddPhotoEquipment.click();
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitTill(2000);
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitTill(2000);
		driver.switchTo().alert().accept();
		ARCFNAAppUtils.waitForElement(driver, btnCameraEquipment, 40);
		btnCameraEquipment.click();
		ARCFNAAppUtils.waitForElement(driver, btnMarkupDraw, 40);
		btnMarkupDraw.click();
		ARCFNAAppUtils.waitForElement(driver, btnLineDraw, 40);
		btnLineDraw.click();
		TouchAction action1 = new TouchAction(driver).longPress(400,184).moveTo(607,571).release().perform();
		ARCFNAAppUtils.waitForElement(driver, btnMarkupDone, 40);
		btnMarkupDone.click();
	}
	
	public void equipmentDetailsAdd(String infoLebel)
	{
		int lblCount = lblEquipmentInfoList.size();
		for(int i = 0; i<=lblCount; i++)
			{
				if(lblEquipmentInfo.getText().equalsIgnoreCase(infoLebel))
					{
					int addCount = btnsEquipmentAllDetailsAdd.size();
						int j = i;
						for(int k=0;k<=addCount;k++)
						{
							if(k==j)
							{
								btnEquipmentAllDetailsAdd.click();
							}
							break;
						}
						
					}
			}
		
		
	}
	
	
	
	
	public void logOff()
	{
		Log.message("Logging out from application");
		ARCFNAAppUtils.waitForElement(driver, btnMenu, 40);
		btnMenu.click();
		Log.message("Menu button clicked");
		ARCFNAAppUtils.waitForElement(driver, btnSignOut, 20);
		btnSignOut.click();
		driver.switchTo().alert().accept();
		Log.message("Successfully Logout");
		ARCFNAAppUtils.waitTill(3000);
	}
	
}

package com.arc.fna.utils;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.arcautoframe.utils.AppiumDriverFactory;
import com.arcautoframe.utils.Log;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SkySiteListener implements ITestListener{
	
	static AppiumDriver<MobileElement> driver;

	@Override
	public void onTestStart(ITestResult result) {
				// TODO Auto-generated method stub
		
		driver = AppiumDriverFactory.get();
		Log.message("FNA app launched successfully");
		try {
			Thread.sleep(8000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}
	

}

package com.arc.fna.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.log4testng.Logger;

import com.arcautoframe.utils.EnvironmentPropertiesReader;

public class LoadPropertiesFromFileSystem {
	
	private static final Logger log = Logger.getLogger(LoadPropertiesFromFileSystem.class);
	private static LoadPropertiesFromFileSystem orProperties;
	
	private Properties properties;

	private LoadPropertiesFromFileSystem() {
		properties = loadProperties();
	}
	
	private Properties loadProperties() {

		//File file = new File("./src/main/resources/config.properties");
		//FileInputStream fileInput = null;
		Properties props = new Properties();
	
		try {
//			fileInput = new FileInputStream(file);			
//			props.load(fileInput);
//			fileInput.close();
			InputStream cpr = LoadPropertiesFromFileSystem.class.getResourceAsStream("/IOSOR.properties");		
			props.load(cpr);
			cpr.close();
			
		} catch (FileNotFoundException e) {
			log.error("config.properties is missing or corrupt : " + e.getMessage());
		} catch (IOException e) {
			log.error("read failed due to: " + e.getMessage());
		}

		return props;
	}
	
	public static LoadPropertiesFromFileSystem getInstance() {
		if (orProperties == null) {
			orProperties = new LoadPropertiesFromFileSystem();
		}
		return orProperties;
	}
	
	public String getProperty(String key) {
		return properties.getProperty(key);
	}
	}
	



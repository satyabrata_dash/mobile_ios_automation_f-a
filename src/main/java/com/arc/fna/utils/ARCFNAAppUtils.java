package com.arc.fna.utils;

import static org.testng.Assert.assertEquals;

import java.awt.image.BufferedImage;
import java.io.File;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.mobile.NetworkConnection;
import org.openqa.selenium.mobile.NetworkConnection.ConnectionType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.arcautoframe.utils.AppiumDriverFactory;
import com.arcautoframe.utils.DataUtils;
import com.arcautoframe.utils.Log;
import com.arcautoframe.utils.StopWatch;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
//import io.appium.java_client.android.Connection;
import io.appium.java_client.android.HasNetworkConnection;

public class ARCFNAAppUtils {
	
	public static int scoutappMaxElementWait = 30;
	public static AppiumDriver<MobileElement> driver;
	static WebDriver cloudDriver;
	
	public static void setScreenOrientation(AppiumDriver<MobileElement> driver,
			String screenOrientationString) {
			if (screenOrientationString==null) {
			Log.message("ScreenOrientation is NOT specified, so skipping setup");
			return;
			}
			Log.message("ScreenOrientation is specified: " + screenOrientationString);
			ScreenOrientation screenOrientation = ScreenOrientation.valueOf(screenOrientationString);
			driver.rotate(screenOrientation);
			if (driver.getOrientation() != screenOrientation) {
				throw new RuntimeException("Changing screenOrientation fail");
			}
	}
	
	/**
	 * Wait driver.getPageSource() util no change. This will be usuful to detect
	 * whether there is some change going or not.
	 * 
	 * @param driver
	 * @return
	 */
	public static String getStablePageSource(AppiumDriver<MobileElement> driver) {
		return getStableString(() -> driver.getPageSource(), 500);
	}
	
	public static String getStableString(Supplier<String> stringSupplier, int interval) {
		String newPs = stringSupplier.get();
		String ps = "";
		while (!ps.equals(newPs)) {
			ps = newPs;
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				throw new RuntimeException(e);
			}
			newPs = stringSupplier.get();
		}
		return newPs;
	}
	
	public static HashMap<String, String> getTestData(String testcaseId, String sheetName) {
		HashMap<String, String> dataToBeReturned = null;
		// get the os and browser details(name and version)
		String tempSheetName = null;
		String configSheetName = null;
		tempSheetName = sheetName;
		// fetching test data sheet names depends on package name
		if (sheetName.contains("tests")) {
			configSheetName = "Smoke";
		}
		// To get the package and class name
		sheetName = tempSheetName.split("\\.")[(tempSheetName.split("\\.").length) - 1];
		// Get the test data from test case sheet
		dataToBeReturned = DataUtils.testDatabyID(testcaseId, sheetName, configSheetName);
		Log.event("All input data:: " + dataToBeReturned);
		return dataToBeReturned;
	}
	
	public static void waitForSpinner(final AppiumDriver<MobileElement> driver) {
		long startTime = StopWatch.startTime();
		try {
			//(new WebDriverWait(driver, 120).pollingEvery(500, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class, StaleElementReferenceException.class).withMessage("EasyBridge spinners/page not loading")).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAActivityIndicator[1]")));
			(new WebDriverWait(driver, 120).pollingEvery(500, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class, StaleElementReferenceException.class).withMessage("EasyBridge spinners/page not loading")).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//XCUIElementTypeTextField")));
		} catch (Exception ex) {
			Log.event("Catched EasyBridge reader spinner load exception");
		}
		Log.event("EasyBridge reader Spinner Load Wait: (Sync)", StopWatch.elapsedTime(startTime));
	}
	
	public static String testDataReading(String testdata) throws IOException
	{
		Properties obj = new Properties(); 
		FileInputStream objfile = new FileInputStream("./src/main/resources/Testdata.properties");
		obj.load(objfile); 
		String getdata = obj.getProperty(testdata);
		return getdata;
	}
	
	public static String objectReading(String testdata) throws IOException
	{
		Properties obj = new Properties(); 
		FileInputStream objfile = new FileInputStream("./src/main/resources/Testdata.properties");
		obj.load(objfile); 
		String getdata = obj.getProperty(testdata);
		return getdata;
	}
	
	public static boolean waitForElement(AppiumDriver<MobileElement> driver, WebElement element) {
		return waitForElement(driver, element, scoutappMaxElementWait);
	}

	/**
	 * To wait for the specific element on the page
	 * 
	 * @param driver
	 *            -
	 * @param element
	 *            - webelement to wait for to appear
	 * @param maxWait
	 *            - how long to wait for
	 * @return boolean - return true if element is present else return false
	 */
	public static boolean waitForElement(AppiumDriver<MobileElement> driver, WebElement element, int maxWait) {
		boolean statusOfElementToBeReturned = false;
		long startTime = StopWatch.startTime();
		WebDriverWait wait = new WebDriverWait(driver, maxWait);
		try {
			WebElement waitElement = wait.until(ExpectedConditions.visibilityOf(element));
			if (waitElement.isDisplayed() && waitElement.isEnabled()) {
				statusOfElementToBeReturned = true;
				Log.event("Element is displayed:: " + element.toString());
				//Log.message("Element is displayed: "+ element.toString());
				//Log.message(element.toString() + "is displayed after: " + StopWatch.elapsedTime(startTime) + " seconds" );
			}
		} catch (Exception ex) {
			statusOfElementToBeReturned = false;
			Log.event("Unable to find an element after " + StopWatch.elapsedTime(startTime) + " sec" );
			//Log.message(element.toString() + "Is not displayed after: " + StopWatch.elapsedTime(startTime) + " seconds" );
		}
		return statusOfElementToBeReturned;
	}
	
	public static boolean waitForElement1(WebDriver cloudDriver, WebElement element, int maxWait) {
		boolean statusOfElementToBeReturned = false;
		long startTime = StopWatch.startTime();
		WebDriverWait wait = new WebDriverWait(cloudDriver, maxWait);
		try {
			WebElement waitElement = wait.until(ExpectedConditions.visibilityOf(element));
			if (waitElement.isDisplayed() && waitElement.isEnabled()) {
				statusOfElementToBeReturned = true;
				//Log.event("Element is displayed:: " + element.toString());
				//Log.message("Element is displayed: "+ element.toString());
				//Log.message(element.toString() + "is displayed after: " + StopWatch.elapsedTime(startTime) + " seconds" );
			}
		} catch (Exception ex) {
			statusOfElementToBeReturned = false;
			//Log.event("Unable to find an element after " + StopWatch.elapsedTime(startTime) + " sec" );
			//Log.message(element.toString() + "Is not displayed after: " + StopWatch.elapsedTime(startTime) + " seconds" );
		}
		return statusOfElementToBeReturned;
	}
	
	
	
	public static boolean waitForListElement(AppiumDriver<MobileElement> driver, List<WebElement> element, int maxWait) {
		boolean statusOfElementToBeReturned = false;
		long startTime = StopWatch.startTime();
		WebDriverWait wait = new WebDriverWait(driver, maxWait);
		try {
			List<WebElement> waitElement = wait.until(ExpectedConditions.visibilityOfAllElements(element));
			if (((WebElement) waitElement).isDisplayed() && ((WebElement) waitElement).isEnabled()) {
				statusOfElementToBeReturned = true;
				Log.event("Element is displayed:: " + element.toString());
			}
		} catch (Exception ex) {
			statusOfElementToBeReturned = false;
			Log.event("Unable to find a element after " + StopWatch.elapsedTime(startTime) + " sec" );
		}
		return statusOfElementToBeReturned;
	}
	
	public static void capabilitySetting()
	{
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("--session-override",true);
	}
	
	public static void logoff()
	{
		driver.findElement(By.xpath("//XCUIElementTypeButton[2][@label='ProfileIcon']")).click();
		driver.findElement(By.xpath("//*[@name='Log Out']")).click();
	}
	
	public static void cropImage(File f1, WebElement e1, String loc) throws IOException
	{
		int ImageWidth = e1.getSize().getWidth() + 500;
		Log.message("******************@@@@"+" "+Integer.toString(ImageWidth));
		int ImageHeight = e1.getSize().getHeight() + 700;
		Log.message("******************@@@@"+" "+Integer.toString(ImageHeight));

		Point point = e1.getLocation();
		int xcord = point.getX();
		Log.message("******************@@@@"+" "+Integer.toString(xcord));
		int ycord = point.getY();
		Log.message("******************@@@@"+" "+Integer.toString(ycord));

		BufferedImage img = ImageIO.read(f1);

		BufferedImage dest = img.getSubimage(xcord, ycord, ImageWidth, ImageHeight);
		ImageIO.write(dest, "png", f1);

		FileUtils.copyFile(f1, new File(loc));
	}
	
	public static void cropImageZoom(File f1, WebElement e1, String loc) throws IOException
	{
		int ImageWidth = e1.getSize().getWidth();
		Log.message("******************@@@@"+" "+Integer.toString(ImageWidth));
		int ImageHeight = e1.getSize().getHeight() + 300;
		Log.message("******************@@@@"+" "+Integer.toString(ImageHeight));

		Point point = e1.getLocation();
		int xcord = point.getX() + 100;
		Log.message("******************@@@@"+" "+Integer.toString(xcord));
		int ycord = point.getY() + 100;
		Log.message("******************@@@@"+" "+Integer.toString(ycord));

		BufferedImage img = ImageIO.read(f1);

		BufferedImage dest = img.getSubimage(xcord, ycord, ImageWidth, ImageHeight);
		ImageIO.write(dest, "png", f1);

		FileUtils.copyFile(f1, new File(loc));
	}
	
	public static void waitTill(int value){
		   try {
			     Thread.sleep(value);
		    } catch (InterruptedException e) {
			     e.printStackTrace();
		      }
	   }
	
	public static void elementHighlighter(WebDriver driver, WebElement element)
	{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
	 }
	
	
	
	public static WebDriver browserAppLaunch(String browserName, String url)
	{
		if(browserName.equals("chrome"))
		{
			cloudDriver = new ChromeDriver();
			//Log.message("Chrome browser is launched successfully");
			cloudDriver.manage().window().fullscreen();
			//Log.message("Browser window is maximized");
		}
		
		cloudDriver.get(url);
		
		return cloudDriver;
	}
	
	public static void deleteCroppedScreenShotFiles() throws IOException
	{
		Files.walk(Paths.get("./FinalCroppedScreenShots"))
        .filter(Files::isRegularFile)
        .map(Path::toFile)
        .forEach(File::delete);
	}
	
	public static void deleteInitialScreenShotFiles() throws IOException
	{
		Files.walk(Paths.get("./InitialScreenShots"))
        .filter(Files::isRegularFile)
        .map(Path::toFile)
        .forEach(File::delete);
	}
	
	public AppiumDriver<MobileElement> initiateAppiumDriver() throws InterruptedException
	{
		//StartStopAppiumServer.startAppiumServer();
		driver = AppiumDriverFactory.get();
		Log.message("FNA app launched successfully");
		Thread.sleep(8000);
		//locationSelectionPage = new LocationSelectionPage(driver).get();
		//locationSelectionPage.clickDontAllow();
		return driver;
	}
	
	/*public static void tapElement(WebElement element)
	{
		TouchAction action = new TouchAction(driver);
		action.press(element).release().perform();
		action.release();
	}*/
	
	
	
	

	
}
	

	




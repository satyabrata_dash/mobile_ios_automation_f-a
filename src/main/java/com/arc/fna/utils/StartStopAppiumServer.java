package com.arc.fna.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;

import com.arcautoframe.utils.EnvironmentPropertiesReader;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

public class StartStopAppiumServer {
	
	public static AppiumDriver<MobileElement> driver;
	public static AppiumDriverLocalService service;
	private static EnvironmentPropertiesReader configProperty = EnvironmentPropertiesReader.getInstance();
	static AppiumDriverLocalService appiumService;
	static String appiumServiceUrl;
	
	public static void startAppiumServer() throws InterruptedException //throws InterruptedException
	{
	
		Map<String, String> env = new HashMap<>(System.getenv());
		env.put("PATH", "/usr/local/Cellar/libimobiledevice/HEAD-3a37a4e_2/lib:" + env.get("PATH"));
		service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
				.usingDriverExecutable(new File(configProperty.getProperty("node.exe_location")))
				.withAppiumJS(new File(configProperty.getProperty("appium.js_location")))
				.withIPAddress(configProperty.getProperty("ipaddress"))
				.usingPort(Integer.parseInt(configProperty.getProperty("port"))));
		
		if(service.isRunning())
		{
			service.stop();
			Thread.sleep(10000);
			service.start();
			Thread.sleep(10000);
		}
		else
		{
			service.start();
			Thread.sleep(10000);
		}
	}
	
	/*CommandLine command = new CommandLine(
			"/Applications/Appium.app/Contents/Resources/node/bin/node");
	command.addArgument(
			"/Applications/Appium.app/Contents/Resources/node_modules/appium/bin/appium.js",
			false);*/
	/*CommandLine command = new CommandLine(configProperty.getProperty("node.exe_location"));
	command.addArgument(configProperty.getProperty("appium.js_location"),false);
	command.addArgument("--address", false);
	command.addArgument("127.0.0.1");
	command.addArgument("--port", false);
	command.addArgument("4723");
	command.addArgument("--full-reset", false);
	DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
	DefaultExecutor executor = new DefaultExecutor();
	executor.setExitValue(1);
	try {
		executor.execute(command, resultHandler);
		Thread.sleep(5000);
		System.out.println("Appium server started.");
	} catch (IOException e) {
		e.printStackTrace();
	} catch (InterruptedException e) {
		e.printStackTrace();
	}
			
	}*/
	
	public static void stopAppiumServer()
	{
		//if(service.isRunning())
			//service.stop();
		String[] command = { "/usr/bin/killall", "-KILL", "node" };
		//String[] command = { "sh", "-c", "lsof -P | grep ':4725' | awk '{print $2}' | xargs kill -9"};
		try {
			Runtime.getRuntime().exec(command);
			System.out.println("Appium server stopped.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	

}

package com.arc.fna.tests;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CollectionListPage;
import com.arc.fna.pages.FNALoginPage;
import com.arc.fna.pages.FoldersNFilesPage;
import com.arc.fna.utils.ARCFNAAppUtils;
import com.arcautoframe.utils.AppiumDriverFactory;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;
import com.arcautoframe.utils.RetryAnalyzer;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

@Listeners(EmailReport.class)

public class FNA_CollectionFolder {
	
	ITestResult result;
	FNALoginPage loginPage;
	CollectionListPage collectionListPage;
	FoldersNFilesPage foldersNFilesPage;
	static AppiumDriver<MobileElement> driver;
	
	@BeforeClass
	public void beforeMethod() throws InterruptedException
	{
		 //StartStopAppiumServer.startAppiumServer();
		driver = AppiumDriverFactory.get();
		Log.message("FNA app launched successfully");
		Thread.sleep(8000);
	}
	
	/*@BeforeMethod
	public void onWifiForceFully()
	{
	 collectionListPage.onWiFiForceFully();
	}*/
	
	@Test(priority = 1, description = "TC_023: Verify Folder Creation", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyFolderCreation(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_023: Verify Folder Creation");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			//collectionListPage.onWiFiForceFully();
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.deleteFolder();
			foldersNFilesPage.clickThreedottedButton();
			Log.assertThat(foldersNFilesPage.createFolder(), "Folder creation successful", "Folder creation failed", driver);
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 2, description = "TC_024: Verify Folder Deletion", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyFolderDeletion(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_024: Verify Folder Deletion");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.deleteFolder();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.createFolder();
			Log.assertThat(foldersNFilesPage.deleteFolder(), "Folder deletion successful", "Folder deletion unsuccessful", driver);
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}

	@Test(priority = 3, description = "TC_025: Verify Upload file inside folder.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyFileUpload(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_025: Verify Upload file inside folder.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.deleteFolder();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.createFolder();
			foldersNFilesPage.clickOnCreatedFolder();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.uploadFile();
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 4, description = "TC_026: Verify Share Folder.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyFolderSharing(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_026: Verify Share Folder.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.deleteFolder();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.createFolder();
			foldersNFilesPage.clickOnCreatedFolder();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.uploadFile();
			foldersNFilesPage.returnFolderPage();
			foldersNFilesPage.clickThreedottedButton();
			Log.assertThat(foldersNFilesPage.shareFolder(), "Folder sharing successful", "Folder sharing unsuccessful", driver);
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 5, description = "TC_027: Verify Move Folder.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyFolderMoveFeature(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_027: Verify Move Folder.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.deleteFolder();
			foldersNFilesPage.deleteFolder1();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.createFolder();
			foldersNFilesPage.clickOnCreatedFolder();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.uploadFile();
			foldersNFilesPage.returnFolderPage();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.createFolder1();
			foldersNFilesPage.clickThreedottedButton();
			Log.assertThat(foldersNFilesPage.moveFolder(), "Move folder functionality working fine", "Move folder functionality is not working fine", driver);
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 6, description = "TC_036: Verify File details would be opened if File Info button clicked.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyFileDetails(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_036: Verify File details would be opened if File Info button clicked.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			Log.assertThat(foldersNFilesPage.verifyFileInfo(), "File details is visible properly", "File details is not visible properly", driver);
			foldersNFilesPage.closeFileInfo();
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 7, description = "TC_037: Verify if no file is synced then it is not allowed to get into Collection while Wi-Fi disabled.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyAllUnsyncedFileOfflineMode(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_037: Verify if no file is synced then it is not allowed to get into Collection while Wi-Fi disabled.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			collectionListPage.offWiFi();
			collectionListPage.clickFNA();
			ARCFNAAppUtils.waitTill(4000);
			collectionListPage.clickProject();
			Log.assertThat(collectionListPage.verifyOpeningProjectWhenWifiOffNoFileSynced(), "Collection is not opened as WiFi is disbaled and no file synced yet.", "Collection is opened though WiFi is disbaled and no file synced yet.", driver);
			collectionListPage.onWiFi();
			collectionListPage.clickFNA();
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 8, description = "TC_038: Verify if even single file is synced then it is allowed to get into that Collection while Wi-Fi disabled.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifySingleSyncedFileOfflineMode(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_038: Verify if even single file is synced then it is allowed to get into Collection while Wi-Fi disabled.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.buttonSyncOffline();
			collectionListPage.openCollectionList();
			collectionListPage.offWiFi();
			collectionListPage.clickFNA();
			collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			Log.assertThat(foldersNFilesPage.verifyCollectionOpenOfflineMode(), "Collection is opened as single file is synced", "Collection is not opened where as single file is synced", driver);
			foldersNFilesPage.buttonSyncOnline();
			collectionListPage.onWiFi();
			collectionListPage.clickFNA();
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 9, description = "TC_043: Verify if no file is synced then it is allowed to get into that Collection while Wi-Fi disabled.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyAllUnSyncedFileOfflineMode(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_043: Verify if no file is synced then it is allowed to get into that Collection while Wi-Fi disabled.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.buttonSyncOffline();
			collectionListPage.offWiFi();
			collectionListPage.clickFNA();
			collectionListPage.openCollectionList();
			collectionListPage.openProject1();
			Log.assertThat(collectionListPage.verifyOpeningProjectWhenWifiOffNoFileSynced(), "collection not opened as no file is synced", "collection getting opened even no file is synced", driver);
			collectionListPage.onWiFi();
			collectionListPage.clickFNA();
			collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.buttonSyncOfflineRemove();
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 10, description = "TC_039: Verify Synced file can be viewed in offline mode also.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyViewSyncFileOfflineMode(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_039: Verify Synced file can be viewed in offline mode also.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.buttonSyncOffline();
			collectionListPage.openCollectionList();
			collectionListPage.offWiFi();
			collectionListPage.clickFNA();
			collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			Log.assertThat(foldersNFilesPage.openSyncedFileOffLineMode(), "Able to view synced file in Offline mode", "Unable to view synced file in offline mode", driver);
			collectionListPage.onWiFi();
			collectionListPage.clickFNA();
			foldersNFilesPage.closeViewer();
			foldersNFilesPage.buttonSyncOfflineRemove();
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			
			Log.endTestCase();
		}
	}
	
	@Test(priority = 11, description = "TC_040: Verify unsynced files can't be viewed in ofline mode.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyViewUnSyncFileOfflineMode(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_040: Verify unsynced files can't be viewed in ofline mode.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			collectionListPage.offWiFi();
			collectionListPage.clickFNA();
			foldersNFilesPage.openFile();
			Log.assertThat(collectionListPage.verifyOpeningProjectWhenWifiOffNoFileSynced(), "Unsynced file is not being opened in Offline mode", "Unsynced file is being opened in Offline mode", driver);
			collectionListPage.onWiFi();
			collectionListPage.clickFNA();
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 12, description = "TC_041: Verify markup saving is not allowed for synced file in offline mode.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifySavingMarkupSyncedFileOfflineMode(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_041: Verify markup saving is not allowed for synced file in offline mode.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.buttonSyncOffline();
			collectionListPage.openCollectionList();
			collectionListPage.offWiFi();
			collectionListPage.clickFNA();
			collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.openSyncedFileOffLineMode();
			foldersNFilesPage.rectangleFileInfoLinkSelection();;
			Log.assertThat(foldersNFilesPage.verifySquareMarkupAnnotationDrawingOffline(), "Markup drawing is not allowd in offline mode", "Markup drawing is allowd in offline mode", driver);
			collectionListPage.onWiFi();
			collectionListPage.clickFNA();
			foldersNFilesPage.closeViewerWithUnsavedMarkups();;
			foldersNFilesPage.buttonSyncOfflineRemove();
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			
			Log.endTestCase();
		}
	}
	
	@Test(priority = 13, description = "TC_042: Verify File syncing can be removed in offline mode.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyFileSyncRemoveOfflineMode(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_042: Verify File syncing can be removed in offline mode.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.buttonSyncOffline();
			collectionListPage.openCollectionList();
			collectionListPage.offWiFi();
			collectionListPage.clickFNA();
			collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.buttonSyncOffline();
			foldersNFilesPage.removeClickedAlertMessage();
			Log.assertThat(foldersNFilesPage.verifyFileSyncRemoval(), "Sync removed for file in offline mode", "Sync removal failed in offline mode", driver);
			collectionListPage.onWiFi();
			collectionListPage.clickFNA();
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			
			Log.endTestCase();
		}
	}
	
	@Test(priority = 14, description = "TC_044: Verify markup deletion is not allowed for synced file in offline mode.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyMarkupDeletionOfflineMode(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_044: Verify markup deletion is not allowed for synced file in offline mode.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.lineMarkupSelection();
			foldersNFilesPage.markupAnnotationDrawing();
			foldersNFilesPage.closeViewer();
			foldersNFilesPage.buttonSyncOffline();
			collectionListPage.openCollectionList();
			collectionListPage.offWiFi();
			collectionListPage.clickFNA();
			collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			Log.assertThat(foldersNFilesPage.deleteMarkupsOffline(), "It is not allowed to delete markups in offline mode", "Markups getting deleted in offline mode", driver);
			foldersNFilesPage.closeViewer();
			foldersNFilesPage.closeViewer();
			foldersNFilesPage.buttonSyncOfflineRemove();
			collectionListPage.onWiFi();
			collectionListPage.clickFNA();
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 15, description = "TC_045: Verify move file (unsynced) in offline mode.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyMoveFileSyncedOfflineMode(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_045: Verify move file (unsynced) in offline mode.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			collectionListPage.openCollectionList();
			collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			collectionListPage.offWiFi();
			collectionListPage.clickFNA();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.clickSelect();
			foldersNFilesPage.selectSyncedFile();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.clickMove();
			Log.assertThat(foldersNFilesPage.verifyMoveSyncedFileOffline(), "In offline mode unsynced file movement is not allowed.", "In offline mode also unsynced file movement is happening.", driver);
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.deSelect();
			collectionListPage.onWiFi();	
			collectionListPage.clickFNA();
			collectionListPage.logOff();
			
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 16, description = "TC_046: Verify move file (synced) in offline mode.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyMoveFileUnSyncedOfflineMode(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_046: Verify move file (synced) in offline mode.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.buttonSyncOffline();
			collectionListPage.openCollectionList();
			collectionListPage.offWiFi();
			collectionListPage.clickFNA();
			collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.clickSelect();
			foldersNFilesPage.selectUnSyncedFile();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.clickMove();
			Log.assertThat(foldersNFilesPage.verifyMoveSyncedFileOffline(), "In offline mode synced file movement is not allowed.", "In offline mode also synced file movement is happening.", driver);
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.deSelect();
			collectionListPage.onWiFi();	
			collectionListPage.clickFNA();
			foldersNFilesPage.buttonSyncOfflineRemove();
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 17, description = "TC_047: Verify move folder (file - synced) in offline mode.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyMoveFolderOfflineMode(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_047: Verify move folder (file - synced) in offline mode.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.buttonSyncOffline();
			collectionListPage.openCollectionList();
			collectionListPage.offWiFi();
			collectionListPage.clickFNA();
			collectionListPage.openProject();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.clickSelect();
			foldersNFilesPage.selectFolder();
			foldersNFilesPage.clickThreedottedButton();
			foldersNFilesPage.clickMove();
			Log.assertThat(foldersNFilesPage.verifyMoveSyncedFileOffline(), "In offline mode folder movement is not allowed.", "In offline mode also folder movement is happening.", driver);
			collectionListPage.onWiFi();	
			collectionListPage.clickFNA();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.buttonSyncOfflineRemove();
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 18, description = "TC_003: Verify rename folder from folder level", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyRenameFolder(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_003: Verify rename folder from folder level");
		loginPage = new FNALoginPage(driver).get();
		try
		{
		collectionListPage = loginPage.loginWithValidCredential();
		foldersNFilesPage = collectionListPage.openProject();
		foldersNFilesPage.deleteModifiedFolder();
		foldersNFilesPage.clickThreedottedButton();
		foldersNFilesPage.createFolderForNameModification();
		foldersNFilesPage.folderNameChange();
		Log.assertThat(foldersNFilesPage.folderNameMatch(), "Folder name changed successfully", "Folder name changed unsuccessful", driver);
		collectionListPage.logOff();
		}
		catch(Exception e) {
		driver.launchApp();
		e.getCause();
		Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 19, description = "TC_048: Verify user will be restricted for creating same name folder.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyFldrCreationDuplicateName(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_048: Verify user will be restricted for creating same name folder.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
		collectionListPage = loginPage.loginWithValidCredential();
		foldersNFilesPage = collectionListPage.openProject();
		foldersNFilesPage.deleteDuplicateFolderTest();
		foldersNFilesPage.clickThreedottedButton();
		foldersNFilesPage.createFolderForNameModification();
		foldersNFilesPage.clickThreedottedButton();
		Log.assertThat(foldersNFilesPage.verifyDuplicateFolderCreation(), "Duplicate folder cration is not allowed", "Duplicate folder is created", driver);
		collectionListPage.logOff();
		}
		catch(Exception e) {
		driver.launchApp();
		e.getCause();
		Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 20, description = "TC_049: Verify user will be restricted for renaming to an existing folder.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyFldrRenameDuplicate(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_049: Verify user will be restricted for renaming to an existing folder.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
		collectionListPage = loginPage.loginWithValidCredential();
		foldersNFilesPage = collectionListPage.openProject();
		foldersNFilesPage.deleteDuplicateFolderTest();
		foldersNFilesPage.clickThreedottedButton();
		foldersNFilesPage.createFolderForNameModification();
		Log.assertThat(foldersNFilesPage.verifyFolderNameChangeForRename(), "Rename folder with existing name is not allowed", "Rename folder with existing name is allowing", driver);
		collectionListPage.logOff();
		}
		catch(Exception e) {
		driver.launchApp();
		e.getCause();
		Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 21, description = "TC_050: Verify user is restricted to move file in same directory", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyMoveFileToSameFldr(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_050: Verify user is restricted to move file in same directory");
		loginPage = new FNALoginPage(driver).get();
		try
		{
		collectionListPage = loginPage.loginWithValidCredential();
		foldersNFilesPage = collectionListPage.openProject();
		foldersNFilesPage.openParentFolder();
		foldersNFilesPage.clickThreedottedButton();
		Log.assertThat(foldersNFilesPage.verifyFileMoveToSameFldr(), "File move is not allowed in same folder", "File move is allowing in same folder", driver);
		collectionListPage.logOff();
		}
		catch(Exception e) {
		driver.launchApp();
		e.getCause();
		Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 22, description = "TC_051: Verify user is restricted to move folder in same directory", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyMoveFldrToSameFldr(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_051: Verify user is restricted to move folder in same directory");
		loginPage = new FNALoginPage(driver).get();
		try
		{
		collectionListPage = loginPage.loginWithValidCredential();
		foldersNFilesPage = collectionListPage.openProject();
		foldersNFilesPage.clickThreedottedButton();
		Log.assertThat(foldersNFilesPage.verifyFldrMoveToSameFldr(), "Folder move to same folder is not allowed", "Folder move to same Folder is happening", driver);
		collectionListPage.logOff();
		}
		catch(Exception e) {
		driver.launchApp();
		e.getCause();
		Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	
	@AfterClass
	public void quit()
	{
		driver.quit();
	}

}

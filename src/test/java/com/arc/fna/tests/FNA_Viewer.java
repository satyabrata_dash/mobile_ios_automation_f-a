package com.arc.fna.tests;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.FNALoginPage;
import com.arc.fna.pages.FoldersNFilesPage;
import com.arc.fna.pages.LocationSelectionPage;
import com.arc.fna.pages.CollectionListPage;
//import com.arc.fna.utils.ARCFNAStgWebLaunchStg;
import com.arc.fna.utils.StartStopAppiumServer;
import com.arcautoframe.utils.AppiumDriverFactory;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;
import com.arcautoframe.utils.RetryAnalyzer;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;

@Listeners(EmailReport.class)

public class FNA_Viewer {

	ITestResult result;
	LocationSelectionPage locationSelectionPage;
	FNALoginPage loginPage;
	CollectionListPage collectionListPage;
	FoldersNFilesPage foldersNFilesPage;
	static AppiumDriver<MobileElement> driver;

	@BeforeClass
	public void beforeMethod() throws InterruptedException {
		// StartStopAppiumServer.startAppiumServer();
		driver = AppiumDriverFactory.get();
		Log.message("FNA app launched successfully");
		Thread.sleep(8000);
	}

	/**
	 * TC_001: Verify whether Rectangle Infolink Square annotations getting saved
	 * properly.
	 * 
	 * @param screenOrientationString
	 * @throws Exception
	 */
	@Test(priority = 1, description = "TC_001: Verify whether Rectangle Infolink Square annotations getting saved properly.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifySquareMarkupAnnotationDrawingSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_001: Verify whether Rectangle Infolink Square annotations getting saved properly.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.rectangleFileInfoLinkSelection();
			foldersNFilesPage.markupAnnotationDrawing();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Square Markup Annotation Saved properly",
					"Square Markup Annotation Saved improperly", driver);
			Thread.sleep(5000);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	/**
	 * TC_002: Verify the Line markup annotation drawing being saved.
	 * 
	 * @param screenOrientationString
	 * @throws Exception
	 */
	@Test(priority = 2, description = "TC_002: Verify the Line markup arrow annotation drawing being saved.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyLineMarkupArrowAnnotationDrawingSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_002: Verify the Line markup arrow annotation drawing being saved.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.lineMarkupSelection();
			foldersNFilesPage.markupAnnotationDrawing();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Line Markup arrow Annotation Saved properly",
					"Line Markup arrow Annotation Saved improperly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 3, description = "TC_003: Verify whether Text Call Out annotations getting saved properly.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyTextMarkupAnnotationDrawingSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_003: Verify whether Text Call Out annotations getting saved properly.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.textMarkupSelection();
			foldersNFilesPage.textMarkupAnnotationDrawing();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(),
					"Text Markup Call out Annotation Saved properly",
					"Text Markup Call out Annotation Saved improperly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 4, description = "TC_004: Verify the Freehand markup annotation drawing being saved.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyFreehandMarkupAnnotationDrawingSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_004: Verify the Freehand markup annotation drawing being saved.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.freehandMarkupSelection();
			foldersNFilesPage.markupAnnotationDrawing();
			// foldersNFilesPage.checkingSavedMarkerMarkup();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Freehand Markup Annotation Saved properly",
					"Marker Markup Annotation Saved improperly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 5, description = "TC_005: Verify the delete all viewer annotation.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyDeleteAllViewerAnnotation(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_005: Verify the delete all viewer annotation.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.takeScreenShotBeforeDrawing();
			foldersNFilesPage.lineMarkupSelection();
			foldersNFilesPage.markupAnnotationDrawing();
			foldersNFilesPage.freehandMarkupSelection();
			foldersNFilesPage.markerMarkupAnnotationDrawing();
			foldersNFilesPage.multiSelectButtonClick();
			foldersNFilesPage.selectAllButtonClick();
			foldersNFilesPage.removeAll();
			foldersNFilesPage.takeScreenShotAfterDelete();
			Log.assertThat(foldersNFilesPage.imageMachingAfterDelete(), "Select All and remove operartion working fine",
					"Select All and remove operartion not working", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	/* Need to work */

	@Test(priority = 6, description = "TC_006: Verify the  Calibrator annotation drawing being saved.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyCalibriMarkupAnnotationDrawingSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_006: Verify the Calibrator markup annotation drawing being saved.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.onlyMarkupDrawing();
			foldersNFilesPage.calibratorDataFillupFeet();
			foldersNFilesPage.refLineCalibratorSaving1Feet();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Calibrator Markup Annotation Saved properly",
					"Calibrator Markup Annotation Saved improperly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	/* Done */

	@Test(priority = 7, description = "TC_007: Verify before rotating file, the Save button should not be enabled.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifySaveButtonBeforeRotating(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_007: Verify before rotating file, the Save button should not be enabled.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.rotationToolbarSelection();
			Log.assertThat(foldersNFilesPage.verifyRotationSaveDisabled(),
					"Save button is found disabled when rotation is not done",
					"Save button is found enabled when rotation is not done", driver);
			foldersNFilesPage.calcelRotation();
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 8, description = "TC_008: Verify whether Square Hyperlink getting saved properly within same folder different file and working properly.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyHyperlinkMarkupAnnotationDrawingSavingSameFolderDiffFile(String screenOrientationString)
			throws Exception {
		Log.testCaseInfo(
				"TC_008: Verify whether Square Hyperlink getting saved properly within same folder different file and working properly.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.hyperlinkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.hyperLinkFileSelectionSameFolder();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Hyperlink Markup Annotation Saved properly",
					"Marker Markup Annotation Saved improperly", driver);
			Log.assertThat(foldersNFilesPage.verifyHyperlinkFunctionlitySameFolderDiffFile(),
					"Hyperlink functionality working properly", "Hyperlink functionality not working properly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 9, description = "TC_009: Verify the Hyperlink functionality working fine with Diff folder & Diff file.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyHyperlinkFunctionalityDiffFolderDiffFile(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_009: Verify the Hyperlink functionality working fine with Diff folder & Diff file.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.hyperlinkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.hyperLinkFileSelectionDiffFolder();
			Log.assertThat(foldersNFilesPage.verifyHyperlinkFunctionlityDiffFolderDiffFile(),
					"Hyperlink functionality working properly", "Hyperlink functionality not working properly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 10, description = "TC_010: Verify the Hyperlink functionality working fine with Diff folder & Same file Diff Version.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyHyperlinkFunctionalityDiffFolderSameFileDiffVersion(String screenOrientationString)
			throws Exception {
		Log.testCaseInfo(
				"TC_010: Verify the Hyperlink functionality working fine with Diff folder & Same file Diff Version.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.hyperlinkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.hyperLinkFileSelectionDiffFolderSameFileDiffVersion();
			Log.assertThat(foldersNFilesPage.verifyHyperlinkFunctionlityDiffFolderSameFileDiffVersion(),
					"Hyperlink functionality working properly", "Hyperlink functionality not working properly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 11, description = "TC_011: Verify the Hyperlink functionality working fine with Diff folder (Parent -> Child) & Diff file.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyHyperlinkFunctionalityDiffFolderPrntChldSameFileDiffVersion(String screenOrientationString)
			throws Exception {
		Log.testCaseInfo(
				"TC_011: Verify the Hyperlink functionality working fine with Diff folder (Parent -> Child) & Diff file.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.hyperlinkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.hyperLinkFileSelectionDiffFolderPrnttoChldDiffFile();
			Log.assertThat(foldersNFilesPage.verifyHyperlinkFunctionlityDiffFolderPrnttoChldDiffFile(),
					"Hyperlink functionality working properly", "Hyperlink functionality not working properly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 12, description = "TC_012: Verify the Hyperlink functionality working fine with Diff folder (Child -> Parent) & Diff file.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyHyperlinkFunctionalityDiffFolderChldPrntSameFileDiffVersion(String screenOrientationString)
			throws Exception {
		Log.testCaseInfo(
				"TC_012: Verify the Hyperlink functionality working fine with Diff folder (Child -> Parent) & Diff file.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openChildFolder();
			;
			foldersNFilesPage.openChildFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.hyperlinkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.hyperLinkFileSelectionChldtoPrnt();
			Log.assertThat(foldersNFilesPage.verifyHyperlinkFunctionlityChldtoPrnt(),
					"Hyperlink functionality working properly", "Hyperlink functionality not working properly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 13, description = "TC_013: Verify the Hyperlink functionality working fine with multiple page.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyHyperlinkFunctionalityMultiPage(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_013: Verify the Hyperlink functionality working fine with multiple page.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openChildFolder1();
			;
			foldersNFilesPage.openChildFile1();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.hyperlinkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.hyperLinkFileSelectionMultiDoc();
			Log.assertThat(foldersNFilesPage.verifyHyperlinkFunctionlityMultiDoc(),
					"Hyperlink functionality working properly", "Hyperlink functionality not working properly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 14, description = "TC_014: Verify the Hyperlink functionality working fine with folder level.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyHyperlinkFunctionalityFolderLevel(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_014: Verify the Hyperlink functionality working fine with folder level.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.hyperlinkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.hyperlinkFolderSelection();
			Log.assertThat(foldersNFilesPage.verifyHyperlinkFolder(), "Folder Hyperlink functionality working properly",
					"Folder Hyperlink functionality not working properly", driver);
			foldersNFilesPage.closeViewerFolderLevel();
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 15, description = "TC_015: Verify that the user should be able to draw multiple calibration ruler on any page of the document.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyMultiCalibriMarkupAnnotationDrawingSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo(
				"TC_015: Verify that the user should be able to draw multiple calibration ruler on any page of the document.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.calibratorDataFillupFeet();
			foldersNFilesPage.hideViewAllMarkup();
			foldersNFilesPage.reCalibratorkMarkupSelection();
			foldersNFilesPage.reHyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.reCalibratorDataFillupFeet();
			foldersNFilesPage.hideViewAllMarkup();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Calibrator Markup Annotation Saved properly",
					"Calibrator Markup Annotation Saved improperly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 16, description = "TC_016: Verify that the user should be able to save combinations of calibration ruler and reference calibrator in one mark ups.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyCalibriMarkupAnnotationDrawingSavingWithReferenceCalibrator(String screenOrientationString)
			throws Exception {
		Log.testCaseInfo(
				"TC_016: Verify that the user should be able to save combinations of calibration ruler and reference calibrator in one mark ups.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.calibratorDataFillupWithoutSave();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.calibratorMesureLineDrawing();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.calibratorMesureRectDrawing();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.calibratorMesureCircleDrawing();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.calibratorMesureFreehandDrawing();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.calibratorMesureAreaDrawing();
			foldersNFilesPage.calibratorWithRefSaving();

			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Calibrator Markup Annotation Saved properly",
					"Calibrator Markup Annotation Saved improperly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 17, description = "TC_017: Verify tapping on calibration ruler on the document, 4 options should get reflected i.e. Settings, delete, clear all, recalibrate", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyTapfunctionalityOnCalibrator(String screenOrientationString) throws Exception {
		Log.testCaseInfo(
				"TC_017: Verify tapping on calibration ruler on the document, 4 options should get reflected i.e. Settings, delete, clear all, recalibrate.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.calibratorDataFillupWithoutSave();
			foldersNFilesPage.tapOnCalibrator();
			Log.assertThat(foldersNFilesPage.verifyCalibratorTapOption(),
					"All the options available after tapping calibrator",
					"All the options are not available after tapping calibrator");
			foldersNFilesPage.closeViewerWithUnsavedMarkups();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 18, description = "TC_018: Verify clicking on recalibrate option of the calibration ruler, user can redefine the measurement unit.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyRecalibrateFunctionality(String screenOrientationString) throws Exception {
		Log.testCaseInfo(
				"TC_018: Verify clicking on recalibrate option of the calibration ruler, user can redefine the measurement unit.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.calibratorDataFillupWithoutSave();
			foldersNFilesPage.tapOnCalibrator();
			Log.assertThat(foldersNFilesPage.verifyRecalibrate(), "Recalibrate functionality working fine",
					"Recalibrate functionality is not working fine");
			foldersNFilesPage.closeViewerWithUnsavedMarkups();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 19, description = "TC_019: For single page file,Verify whether unit of all the measurment calibrator is getting changed or not with respect to Active calibrator ruler.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyActivecalibrateFunctionality(String screenOrientationString) throws Exception {
		Log.testCaseInfo(
				"TC_019: For single page file,Verify whether unit of all the measurment calibrator is getting changed or not with respect to Active calibrator ruler.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.calibratorDataFillupFeet();
			foldersNFilesPage.hideViewAllMarkup();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing1();
			foldersNFilesPage.calibratorDataFillupMeter();
			foldersNFilesPage.hideViewAllMarkup();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.calibratorMesureLineDrawing();
			foldersNFilesPage.refLineCalibratorSaving1();
			Log.assertThat(foldersNFilesPage.verifyRefLineCalibratorMetric(),
					"Metric calibrator drawing is taking place as Metric is activated",
					"Metric calibrator drawing is not taking place though Metric is activated", driver);
			foldersNFilesPage.tapOnCalibrator();
			foldersNFilesPage.setActiveCalibrator();
			Log.assertThat(foldersNFilesPage.verifyRefLineCalibratorImperial(),
					"Imperial calibrator drawing is taking place as Imperial is activated",
					"Imperial calibrator drawing is not taking place though Imperial is activated", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 20, description = "TC_020: Verify whether latest created calibrator ruler is remain active by default or not.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyActiveFunctionalityLastDrawnCalibrator(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_020: Verify whether latest created calibrator ruler is remain active by default or not.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.calibratorDataFillupFeet();
			foldersNFilesPage.hideViewAllMarkup();
			foldersNFilesPage.reCalibratorkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing1();
			foldersNFilesPage.calibratorDataFillupMeter();
			foldersNFilesPage.hideViewAllMarkup();
			Log.assertThat(foldersNFilesPage.tapOnCalibrator1(), "Last drawn calibrator found in active state",
					"Last drawn calibrator is found in inactive state", driver);
			;
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 21, description = "TC_021: Verify tapping on calibrator from toolbar, Only calibration ruler will be active to draw and rests will be inactive", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyInitialCalibratorRulerVisibility(String screenOrientationString) throws Exception {
		Log.testCaseInfo(
				"TC_021: Verify tapping on calibrator from toolbar, Only calibration ruler will be active to draw and rests will be inactive");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.clickOnCalibrateRuler();
			Log.assertThat(foldersNFilesPage.initialCalibratorRulerVisibility(),
					"Only Calibrator button is in active state at initial stage.",
					"Not only Calibrator button is in active state at initial stage.", driver);
			;
			foldersNFilesPage.closeViewer();
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 22, description = "TC_022: Verify reference calibrator can't be drawn without ruler.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyRefCalibratorDrawingWithoutRuler(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_022: Verify reference calibrator can't be drawn without ruler.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.calibratorDataFillupFeet();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.calibratorMesureLineDrawing();
			foldersNFilesPage.refLineCalibratorSaving1();
			foldersNFilesPage.hideViewAllMarkup();
			foldersNFilesPage.clickOnCalibrateRuler();
			Log.assertThat(foldersNFilesPage.verifyStateRefCalibratorWithoutRuler(),
					"Without ruler ref calibrators are disabled", "Without ruler ref calibrators are also enabled",
					driver);
			foldersNFilesPage.closeViewer();
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 23, description = "TC_023: Verify Saving the ruler and reference calibrators in a separate mark ups.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifySavingRulerRefCalibratorDiffMarkup(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_023: Verify Saving the ruler and reference calibrators in a separate mark ups.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.calibratorDataFillupFeet();
			foldersNFilesPage.hideViewAllMarkup();
			foldersNFilesPage.hideViewAllMarkup();
			foldersNFilesPage.clickOnCalibrateRuler();
			foldersNFilesPage.calibratorMesureLineDrawing();
			foldersNFilesPage.calibratorWithRefSaving();
			Log.assertThat(foldersNFilesPage.verifyRulerNRefCalibratorInDiffMarkup(),
					"Ruler and Ref Calibrator being saved properly in differnet markups",
					"Ruler and Ref Calibrator not being saved properly in differnet markups", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 24, description = "TC_024: Verify Document Export with Calibrator.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyDocumentExportWithCalibrator(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_024: Verify Document Export with Calibrator.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.calibratorkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.calibratorDataFillupFeet();
			foldersNFilesPage.hideViewAllMarkup();
			foldersNFilesPage.hideViewAllMarkup();
			foldersNFilesPage.clickOnCalibrateRuler();
			foldersNFilesPage.calibratorMesureLineDrawing();
			foldersNFilesPage.calibratorWithRefSaving();
			Log.assertThat(foldersNFilesPage.verifyDocumentExportWithCalibrator(),
					"Document exported successfully with Calibrator", "Document exported failed with Calibrator",
					driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 25, description = "TC_025: Verify Rotated state can be cancelled befor saving.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyCancelRotationBeforeSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_025: Verify Rotated state can be cancelled befor saving.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.rotationToolbarSelection();
			foldersNFilesPage.rotateLeft();
			foldersNFilesPage.calcelRotation();
			Log.assertThat(foldersNFilesPage.verifyRotationoptionAvailable(), "Rotation cancelled successfully",
					"Rotation cancelled unsuccessful", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 26, description = "TC_026: Verify whether Rotation getting saved properly.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyRotationSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_026: Verify whether Rotation getting saved properly.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			Log.assertThat(foldersNFilesPage.verifyRotationSaving(), "File is being rotated successfully",
					"File is not being rotated successfully", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 27, description = "TC_027: Verify whether Circle Infolink Square annotations getting saved properly.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyCircleMarkupAnnotationDrawingSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_027: Verify whether Circle Infolink Square annotations getting saved properly.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.circleFileInfoLinkSelection();
			foldersNFilesPage.markupAnnotationDrawing();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Circle Markup Annotation Saved properly",
					"Circle Markup Annotation Saved improperly", driver);
			Thread.sleep(5000);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 28, description = "TC_028: Verify whether Cloud Infolink Square annotations getting saved properly.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyCloudMarkupAnnotationDrawingSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_028: Verify whether Cloud Infolink Square annotations getting saved properly.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.cloudFileInfoLinkSelection();
			foldersNFilesPage.markupAnnotationDrawing();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Cloud Markup Annotation Saved properly",
					"cloud Markup Annotation Saved improperly", driver);
			Thread.sleep(5000);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 29, description = "TC_029: Verify the Line markup line annotation drawing being saved.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyLineMarkupLineAnnotationDrawingSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_029: Verify the Line markup line annotation drawing being saved.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.lineMarkupSelection();
			foldersNFilesPage.lineMarkuplineSelection();
			foldersNFilesPage.markupAnnotationDrawing();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Line Markup line Annotation Saved properly",
					"Line Markup line Annotation Saved improperly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 30, description = "TC_030: Verify whether Marker markup annotations getting saved properly.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyMarkerMarkupAnnotationDrawingSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_030: Verify whether Marker markup annotations getting saved properly.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.markerMarkupSelection();
			foldersNFilesPage.markupAnnotationDrawing();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Marker Markup Annotation Saved properly",
					"Marker Markup Annotation Saved improperly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 31, description = "TC_031: Verify whether Text Infolink annotations getting saved properly.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyTextInfoMarkupAnnotationDrawingSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_031: Verify whether Text Infolink annotations getting saved properly.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.textInfoMarkupSelection();
			foldersNFilesPage.textMarkupNoteAnnotationDrawing1();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Text Markup text Annotation Saved properly",
					"Text Markup text Annotation Saved improperly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 32, description = "TC_032: Verify whether Text Note Infolink annotations getting saved properly.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyTextNoteInfoMarkupAnnotationDrawingSaving(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_032: Verify whether Text Note Infolink annotations getting saved properly.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.textInfoNoteMarkupSelection();
			foldersNFilesPage.textMarkupNoteAnnotationDrawing1();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Text Markup Note Annotation Saved properly",
					"Text Markup Note Annotation Saved improperly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 33, description = "TC_033: Verify whether Circle Hyperlink getting saved properly within same folder different file and working properly.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyCircleHyperlinkMarkupAnnotationDrawingSavingSameFolderDiffFile(String screenOrientationString)
			throws Exception {
		Log.testCaseInfo(
				"TC_033: Verify whether Circle Hyperlink getting saved properly within same folder different file and working properly.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.hyperlinkCircleMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.hyperLinkFileSelectionSameFolder();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(),
					"Hyperlink Markup Circle Annotation Saved properly",
					"Marker Markup Circle Annotation Saved improperly", driver);
			Log.assertThat(foldersNFilesPage.verifyHyperlinkFunctionlitySameFolderDiffFile(),
					"Circle Hyperlink functionality working properly",
					"Hyperlink Circle functionality not working properly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 34, description = "TC_034: Verify whether Cloud Hyperlink getting saved properly within same folder different file and working properly.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyCloudHyperlinkMarkupAnnotationDrawingSavingSameFolderDiffFile(String screenOrientationString)
			throws Exception {
		Log.testCaseInfo(
				"TC_034: Verify whether Cloud Hyperlink getting saved properly within same folder different file and working properly.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.hyperlinkCloudMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.hyperLinkFileSelectionSameFolder();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(),
					"Hyperlink Markup Circle Annotation Saved properly",
					"Marker Markup Circle Annotation Saved improperly", driver);
			Log.assertThat(foldersNFilesPage.verifyHyperlinkFunctionlitySameFolderDiffFile(),
					"Circle Hyperlink functionality working properly",
					"Hyperlink Circle functionality not working properly", driver);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 35, description = "TC_035: Verify external hyperlink functionality.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyExternalHyperlink(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_035: Verify external hyperlink functionality.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.hyperlinkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.clickExternalLink();
			foldersNFilesPage.linkPaste();
			foldersNFilesPage.clickLinkbuttonExternal();
			foldersNFilesPage.savingAnnotation();
			foldersNFilesPage.hideViewAllMarkup();
			foldersNFilesPage.hideViewAllMarkup();
			Log.assertThat(foldersNFilesPage.verifyExternalHyperlink(),
					"External hyperlink functionality working properly",
					"External hyperlink functionality is not working properly", driver);
			foldersNFilesPage.closeViewer();
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	/**
	 * TC_038: Verify whether Rectangle Infolink Square annotations getting saved
	 * properly with zooming functionality.
	 * 
	 * @param screenOrientationString
	 * @throws Exception
	 */
	@Test(priority = 36, description = "TC_038: Verify whether Rectangle Infolink Square annotations getting saved properly with zooming functionality.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifySquareMarkupAnnotationDrawingSavingWithZoom(String screenOrientationString) throws Exception {
		Log.testCaseInfo(
				"TC_038: Verify whether Rectangle Infolink Square annotations getting saved properly with zooming functionality.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.zoomingFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.rectangleFileInfoLinkSelection();
			foldersNFilesPage.markupAnnotationDrawing();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(),
					"Square Markup Annotation Saved properly with zoom",
					"Square Markup Annotation Saved improperly with zoom", driver);
			Thread.sleep(3000);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 37, description = "TC_039: Verify the Line markup arrow annotation drawing being saved with zooming functionality.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyLineMarkupArrowAnnotationDrawingSavingWithZoom(String screenOrientationString) throws Exception {
		Log.testCaseInfo(
				"TC_039: Verify the Line markup arrow annotation drawing being saved with zooming functionality.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.zoomingFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.lineMarkupSelection();
			foldersNFilesPage.markupAnnotationDrawing();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(),
					"Line Markup Annotation Saved properly with zoom",
					"Square Markup Annotation Saved improperly with zoom", driver);
			Thread.sleep(3000);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();

		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 38, description = "TC_040: Verify whether Text Call Out annotations getting saved properly with zooming functionality.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyTextMarkupAnnotationDrawingSavingWithZoom(String screenOrientationString) throws Exception {
		Log.testCaseInfo(
				"TC_040: Verify whether Text Call Out annotations getting saved properly with zooming functionality.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.zoomingFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.textMarkupSelection();
			foldersNFilesPage.textMarkupAnnotationDrawing();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(),
					"Text call out Markup Annotation Saved properly with zoom",
					"Text call out Markup Annotation Saved improperly with zoom", driver);
			Thread.sleep(3000);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();

		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 39, description = "TC_041: Verify whether Freehand annotations getting saved properly with zooming functionality.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyFreehandMarkupAnnotationDrawingSavingWithZoom(String screenOrientationString) throws Exception {
		Log.testCaseInfo(
				"TC_041: Verify whether Freehand annotations getting saved properly with zooming functionality.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.zoomingFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.freehandMarkupSelection();
			foldersNFilesPage.markupAnnotationDrawing();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(),
					"Freehand Markup Annotation Saved properly with zoom",
					"Freehand Markup Annotation Saved improperly with zoom", driver);
			Thread.sleep(2000);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();

		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 40, description = "TC_042: Verify whether Square Hyperlink getting saved properly within same folder different file and working properly with zooming functionality.", enabled = false, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyHyperlinkMarkupAnnotationDrawingSavingSameFolderDiffFileWithZoom(String screenOrientationString)
			throws Exception {
		Log.testCaseInfo(
				"TC_042: Verify whether Square Hyperlink getting saved properly within same folder different file and working properly with zooming functionality.");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.zoomingFile();
			foldersNFilesPage.deleteMarkups();
			foldersNFilesPage.hyperlinkMarkupSelection();
			foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			foldersNFilesPage.hyperLinkFileSelectionSameFolder();
			Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(),
					"Square Hyperlink Markup Annotation Saved properly with zoom",
					"Square Hyperlink Markup Annotation Saved improperly with zoom", driver);
			Log.assertThat(foldersNFilesPage.verifyHyperlinkFunctionlitySameFolderDiffFile(),
					"Hyperlink functionality working properly", "Hyperlink functionality not working properly", driver);
			Thread.sleep(2000);
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();

		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	@Test(priority = 41, description = "TC_064: Verify that user can change the icon for each tag from device", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyChangeTagIcon(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_064: Verify that user can change the icon for each tag from device");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.openTagCreationPopup();
			foldersNFilesPage.changeIcon("Air Conditioner");
			// foldersNFilesPage.zoomingFile();
			// foldersNFilesPage.deleteMarkups();
			// foldersNFilesPage.hyperlinkMarkupSelection();
			// foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			// foldersNFilesPage.hyperLinkFileSelectionSameFolder();
			// Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Square Hyperlink
			// Markup Annotation Saved properly with zoom", "Square Hyperlink Markup
			// Annotation Saved improperly with zoom",driver);
			// Log.assertThat(foldersNFilesPage.verifyHyperlinkFunctionlitySameFolderDiffFile(),
			// "Hyperlink functionality working properly", "Hyperlink functionality not
			// working properly", driver);
			Log.assertThat(foldersNFilesPage.verifyIcon("Air Conditioner"), "Changed icon displayed",
					"Icon did not change");
			Thread.sleep(2000);
			foldersNFilesPage.closeTagPopup();
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}
	@Test(priority = 42, description = "TC_065: Verify that user can search the tag icon and then can select it for each tag from device", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifySearchTagIcon(String screenOrientationString) throws Exception {
		Log.testCaseInfo("TC_065: Verify that user can search the tag icon and then can select it for each tag from device");
		loginPage = new FNALoginPage(driver).get();
		try {
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openProject();
			foldersNFilesPage.openParentFolder();
			foldersNFilesPage.openFile();
			foldersNFilesPage.openTagCreationPopup();
			foldersNFilesPage.searchIcon("Air Handler");
			// foldersNFilesPage.zoomingFile();
			// foldersNFilesPage.deleteMarkups();
			// foldersNFilesPage.hyperlinkMarkupSelection();
			// foldersNFilesPage.hyperlinkMarkupAnnotationDrawing();
			// foldersNFilesPage.hyperLinkFileSelectionSameFolder();
			// Log.assertThat(foldersNFilesPage.checkingSavedMarkupZoom(), "Square Hyperlink
			// Markup Annotation Saved properly with zoom", "Square Hyperlink Markup
			// Annotation Saved improperly with zoom",driver);
			// Log.assertThat(foldersNFilesPage.verifyHyperlinkFunctionlitySameFolderDiffFile(),
			// "Hyperlink functionality working properly", "Hyperlink functionality not
			// working properly", driver);
			Log.assertThat(foldersNFilesPage.verifyIcon("Air Handler"), "Changed icon displayed",
					"Icon did not change");
			Thread.sleep(2000);
			foldersNFilesPage.closeTagPopup();
			foldersNFilesPage.closeViewer();
			collectionListPage.logOff();
		} catch (Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
		}
	}

	/*
	 * @AfterClass public void stopAppiumServer() {
	 * StartStopAppiumServer.stopAppiumServer(); //driver.resetApp();
	 * //driver.closeApp(); }
	 */

	@AfterClass
	public void quit() {
		driver.quit();
	}

}

package com.arc.fna.tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CollectionListPage;
import com.arc.fna.pages.FNALoginPage;
import com.arcautoframe.utils.AppiumDriverFactory;
import com.arcautoframe.utils.Log;
import com.arcautoframe.utils.RetryAnalyzer;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class FNA_CollectionList {
	
	FNALoginPage loginPage;
	CollectionListPage collectionListPage;
	static AppiumDriver<MobileElement> driver;
	
	@BeforeClass
	public void beforeMethod() throws InterruptedException
	{
		driver = AppiumDriverFactory.get();
		Log.message("FNA app launched successfully");
		Thread.sleep(8000);
		 //locationSelectionPage = new LocationSelectionPage(driver).get();
		//locationSelectionPage.clickDontAllow();
	}
	
	/*@BeforeMethod
	public void onWifiForceFully()
	{
	 collectionListPage.onWiFiForceFully();
	}*/
	
	@Test(priority = 1, description = "TC_034: Verify after login user is landed to Collection List screen.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyLandingPageAfterLogin(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_028: Verify after login user is landed to Project List screen.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			Log.assertThat(collectionListPage.presenceOfCollectionList(), "User is landed in Collection List page", "User is not landed in Collection List page", driver);
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 2, description = "TC_035: Verify if no favourite project is there, user would be redirected to All page.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyLandingPageAfterLoginWithNoFavourite(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_029: Verify if no favourite project is there, user would be redirected to All page.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			Log.assertThat(collectionListPage.btnAllEnabled(), "User landed to All page as no favorite project is there.", "User landed to All page even of having favorite projects",driver);
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			//driver.resetApp();
			//driver.closeApp();
			//collectionListPage.logOff();
			//driver.launchApp();
		}
	}
	
	@Test(priority = 3, description = "TC_036: Verify if favourite project is there, user would be redirected to favourite page only.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyLandingPageAfterLoginWithFavouriteProjects(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_030: Verify if favourite project is there, user would be redirected to favourite page only.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			collectionListPage.clickUnFavorite();
			collectionListPage.logOff();
			collectionListPage = loginPage.loginWithValidCredential();
			Log.assertThat(collectionListPage.btnFavoriteEnabled(), "User landed successfully in Favorite page when single Project found favorite", "User is not landed successfully in Favorite page when single Project found favorite", driver);
			collectionListPage.clickFavorite();
			collectionListPage.logOff();
			
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			//driver.resetApp();
			//driver.closeApp();
			//collectionListPage.logOff();
			//driver.launchApp();
		}
	}
	
	@Test(priority = 4, description = "TC_037: Verify tapping on Project info icon, user is able to enter Project Details screen.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyProjectInfoEnteringSaving(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_037: Verify tapping on Project info icon, user is able to enter Project Details screen.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			collectionListPage.SaveProjectInfo();
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			//driver.resetApp();
			//driver.closeApp();
			//collectionListPage.logOff();
			//driver.launchApp();
		}
	}
	
	@AfterClass
	public void quit()
	{
		driver.quit();
	}

}

package com.arc.fna.tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CollectionListPage;
import com.arc.fna.pages.EquipmentPage;
import com.arc.fna.pages.FNALoginPage;
import com.arc.fna.pages.FoldersNFilesPage;
import com.arcautoframe.utils.AppiumDriverFactory;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;
import com.arcautoframe.utils.RetryAnalyzer;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

@Listeners(EmailReport.class)

public class FNA_Equipment {
	
	FNALoginPage loginPage;
	CollectionListPage collectionListPage;
	EquipmentPage equipmentPage;
	FoldersNFilesPage foldersNFilesPage;
	static AppiumDriver<MobileElement> driver;
	
	@BeforeClass
	public void beforeMethod() throws InterruptedException
	{
		driver = AppiumDriverFactory.get();
		Log.message("FNA app launched successfully");
	}
	
	@Test(priority = 1, description = "TC_001: Verify Equipment Management launch in FNA.", enabled = false/*, retryAnalyzer = RetryAnalyzer.class*/)
	@Parameters("screenOrientation")
	public void verifyEquipmentModuleLaunch(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_001: Verify Equipment Management launch in FNA.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openTestAutomationProject();
			foldersNFilesPage.menuButtonClick();
			equipmentPage = foldersNFilesPage.launchEquipment();
			Log.assertThat(equipmentPage.verifyEquipmentModulelaunch(), "Equipment management launched successfully", "Equipment management failed to launche",driver);
			equipmentPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@Test(priority = 2, description = "TC_002: Verify addition of an equipment with all the required fields.", enabled = true/*, retryAnalyzer = RetryAnalyzer.class*/)
	@Parameters("screenOrientation")
	public void verifyEquipmentAdd(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_002: Verify addition of an equipment with all the required fields.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			foldersNFilesPage = collectionListPage.openTestAutomationProject();
			foldersNFilesPage.menuButtonClick();
			equipmentPage = foldersNFilesPage.launchEquipment();
			equipmentPage.equipmentAdd();
			equipmentPage.equipmentPhotoAddWithLineMarkup();
			equipmentPage.equipmentDetailsAdd("Equipment details");
			
	
			
			
			Log.assertThat(equipmentPage.verifyEquipmentModulelaunch(), "Equipment added successfully", "Equipment addtion failed",driver);
			equipmentPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	
	
	@AfterClass
	public void quit()
	{
		driver.quit();
	}
	
	
}
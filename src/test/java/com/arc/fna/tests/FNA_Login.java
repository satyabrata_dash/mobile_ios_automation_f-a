package com.arc.fna.tests;


import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.fna.pages.CollectionListPage;
import com.arc.fna.pages.FNALoginPage;
import com.arc.fna.utils.SkySiteListener;
import com.arcautoframe.utils.AppiumDriverFactory;
import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;
import com.arcautoframe.utils.RetryAnalyzer;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

@Listeners(EmailReport.class)


public class FNA_Login{
	
	FNALoginPage loginPage;
	static AppiumDriver<MobileElement> driver;
	CollectionListPage collectionListPage;
	
	
	@BeforeClass
	public void beforeMethod() throws InterruptedException
	{
		 //StartStopAppiumServer.startAppiumServer();
		driver = AppiumDriverFactory.get();
		Log.message("FNA app launched successfully");
		Thread.sleep(8000);
	}
	
	
	/** TC_007: Verify user should be able to login successfully with valid credential.
	 * 
	 * @param screenOrientationString
	 * @throws Exception
	 */
	@Test(priority = 1, description = "TC_007: Verify user should be able to login successfully with valid credential.", enabled = true, retryAnalyzer = RetryAnalyzer.class)
	@Parameters("screenOrientation")
	public void verifyValidLogin(String screenOrientationString) throws Exception
	{
		Log.testCaseInfo("TC_008: Verify user should be able to login successfully with valid credential.");
		loginPage = new FNALoginPage(driver).get();
		try
		{
			collectionListPage = loginPage.loginWithValidCredential();
			Log.assertThat(collectionListPage.presenceOfMenuButton(), "Login with valid credential successful.", "Login with valid credential unsuccessful", driver);	
			collectionListPage.logOff();
		}
		catch(Exception e) {
			driver.launchApp();
			e.getCause();
			Log.exception(e, driver);
			//driver.resetApp();
		}
		finally
		{
			Log.endTestCase();
		}
	}
	
	@AfterClass
	public void quit()
	{
		driver.quit();
		//driver.close();
	}
	

}
